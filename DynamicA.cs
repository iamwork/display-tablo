﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO.Ports;
using System.Timers;



namespace WindowsFormsApplication1
{
    class DynamicA
    {


  bool debuging = true;


//------------------------------

bool SET_L2 = false;

 // int REGION_AREA = -1; 

int Bold = 0;

 bool testdata = true;
 // bool testdata = false; 


//---------------------------------ПОДКЛЮЧЕНИЕ БИБЛИОТЕКИ-----------------------------------------------------------------------------------------------------------------------

    #region Подключение библиотеки
  

        [DllImport("LedDynamicArea.dll")]
        public static extern int Initialize();

        [DllImport("LedDynamicArea.dll")]
        public static extern int AddScreen_Dynamic(int nControlType, int nScreenNo, int nSendMode, int nWidth, int nHeight,
              int nScreenType, int nPixelMode, string pCom, int nBaud, string pSocketIP, int nSocketPort, int nServerMode,
              string pBarcode, string pNetworkID, string pServerIP, int nServerPort, string pServerAccessUser, string pServerAccessPassword,
              string pCommandDataFile);


        [DllImport("LedDynamicArea.dll")]
        public static extern int AddScreenDynamicArea(int nScreenNo, int nDYAreaID, int nRunMode,
            int nTimeOut, int nAllProRelate, string pProRelateList, int nPlayImmediately,
            int nAreaX, int nAreaY, int nAreaWidth, int nAreaHeight, int nAreaFMode, int nAreaFLine, int nAreaFColor,
            int nAreaFStunt, int nAreaFRunSpeed, int nAreaFMoveStep);


        [DllImport("LedDynamicArea.dll")]
        public static extern int AddScreenDynamicAreaText(int nScreenNo, int nDYAreaID,
        string pText, int nShowSingle, string pFontName, int nFontSize, int nBold, int nFontColor,
        int nStunt, int nRunSpeed, int nShowTime);

        [DllImport("LedDynamicArea.dll")]
        public static extern int SendDynamicAreaInfoCommand(int nScreenNo, int nDYAreaID);


        [DllImport("LedDynamicArea.dll")]
        public static extern int SendDeleteDynamicAreasCommand(int nScreenNo, int nDelAllDYArea, string pDYAreaIDList);


        [DllImport("LedDynamicArea.dll")]
        public static extern int DeleteScreen_Dynamic(int nScreenNo);


        [DllImport("LedDynamicArea.dll")]
            public static extern int AddScreenDynamicAreaFile(int nScreenNo, int nDYAreaID,
            string pFileName, int nShowSingle, string pFontName, int nFontSize, int nBold, int nFontColor,
            int nStunt, int nRunSpeed, int nShowTime);

        [DllImport("LedDynamicArea.dll")]
        public static extern int DeleteScreenDynamicAreaFile(int nScreenNo, int nDYAreaID, int nFileOrd);


        [DllImport("LedDynamicArea.dll")]
        public static extern int Uninitialize();


    #endregion

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//


        public int Start_DynamicA(int ID_TABLO, int WEIGHT_TABLO, int HIGHT_TABLO, string IP_TABLO, int PORT_TABLO) 
{
            int Result = 0;

            if (SET_L2 == false)
            {
                Result = Initialize();

                SET_L2 = true;
            }

            Result = AddScreen_Dynamic(0x0154, ID_TABLO, 2, WEIGHT_TABLO, HIGHT_TABLO,
                                        1, 0x01, "", 0, IP_TABLO, PORT_TABLO, 0,
                                         "", "", "", 0, "", "", "");


            return Result;
}



//---------------------------------ДОБАВЛЕНИЕ ДИНАМИЧЕСКИХ ОБЛАСТЕЙ----------------------------------------------------------------------------------------------------------------//


 //-----------------------------------------ОБЛАСТЬ----------------------------------------------------------------------------------------------------------------------------//
   


public int Add_dArea (int ID_TABLO, int REGION_AREA, int dArea_X, int dArea_Y, int dArea_W, int dArea_H)   

 {
     int Result = 0; 

     Result = AddScreenDynamicArea(ID_TABLO, REGION_AREA, 4, 10, 0, "", 0, dArea_X, dArea_Y, dArea_W, dArea_H, 255, 0, 255, 1, 6, 1); if (debuging) { Console.Write("AddScreenDynamicArea_0 = " + Result); }

     return Result;

}


//-----------------------------------------ТЕКСТ----------------------------------------------------------------------------------------------------------------------------//
   


public int Add_dAreaText (int ID_TABLO, int REGION_AREA, string PARAMETR, string dFont, int dSizeFont )   

 {

 int Result = 0;

 Result = AddScreenDynamicAreaText(ID_TABLO, REGION_AREA, PARAMETR, 0, dFont, dSizeFont, Bold, 65535, 1, 8, 5); /*********/ if (debuging) { Console.Write("AddScreenDynamicAreaText_0 = " + Result); }

 Result = dArea_SendInfo(ID_TABLO, REGION_AREA);



 return Result;

}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//





          

//------------------------------ОТПРАВКА ДИНАМИЧЕСКИХ ОБЛАСТЕЙ---------------------------------------------------------------------------------------------------------------------//



 public int dArea_SendInfo(int ID_TABLO,int REGION)
 {
     int Result = 0;

     try
     {
         if (testdata == false)
         {

             Result = SendDynamicAreaInfoCommand(ID_TABLO, REGION);

         }
     }

     catch { /*return Result = 0; */};  
 
  return Result;

 }



//-------------------------------УДАЛЕНИЕ ДИНАМИЧЕСКИХ ОБЛАСТЕЙ---------------------------------------------------------------------------------------------------------------------------------------------------//





 public int Delete_dAreas(int ID_TABLO)
 {
       
     int Result = 0;

     try
     {
         if (testdata == false)
         {

             Result = SendDeleteDynamicAreasCommand(ID_TABLO, 0, "");
             Result = SendDeleteDynamicAreasCommand(ID_TABLO, 1, "");
             Result = SendDeleteDynamicAreasCommand(ID_TABLO, 2, "");
             Result = SendDeleteDynamicAreasCommand(ID_TABLO, 3, "");
         }
         
     }
     catch { };


     return Result;
   
 }


//-------------------------------УДАЛЕНИЕ ФАЙЛА----------------------------------------------------------------------------------------------------------------------------------



 public int DeleteDynamicAreaFile(int id_T, int area_T)

 {

  int Result = 0;

  Result = DeleteScreenDynamicAreaFile(id_T, area_T, 0); 
   
  return Result;


    }
//--------------------------------УДАЛЕНИЕ ЭКРАНА------------------------------------------------------------------------------------------------------------------


 public int Delete_dScreen(int ID_TABLO)
 {

  int Result = 0;

  Result = DeleteScreen_Dynamic(ID_TABLO);

  return Result;


    }



//-----------------------------------УДАЛЕНИЕ БИБЛИОТЕКИ---------------------------------------------------------------------------------------------------



 public int Dellete_dLib()  
 {

     int Result = 0;

     
    if (SET_L2 == true)
    {
        Result = Uninitialize();
 
        SET_L2 = false;
    }



    return Result;

 }

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//




    }
}
