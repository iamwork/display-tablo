﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.WRez = new System.Windows.Forms.CheckBox();
            this.SizeFont_WRez_T6 = new System.Windows.Forms.TextBox();
            this.Font_WRez_T6 = new System.Windows.Forms.TextBox();
            this.H_WRez_T6 = new System.Windows.Forms.TextBox();
            this.W_WRez_T6 = new System.Windows.Forms.TextBox();
            this.Y_WRez_T6 = new System.Windows.Forms.TextBox();
            this.X_WRez_T6 = new System.Windows.Forms.TextBox();
            this.SizeFont_W_T6 = new System.Windows.Forms.TextBox();
            this.Font_W_T6 = new System.Windows.Forms.TextBox();
            this.H_W_T6 = new System.Windows.Forms.TextBox();
            this.W_W_T6 = new System.Windows.Forms.TextBox();
            this.Y_W_T6 = new System.Windows.Forms.TextBox();
            this.X_W_T6 = new System.Windows.Forms.TextBox();
            this.Add_W = new System.Windows.Forms.CheckBox();
            this.TempRez_T6 = new System.Windows.Forms.CheckBox();
            this.SizeFont_TempRez_T6 = new System.Windows.Forms.TextBox();
            this.Font_TempRez_T6 = new System.Windows.Forms.TextBox();
            this.H_TempRez_T6 = new System.Windows.Forms.TextBox();
            this.W_TempRez_T6 = new System.Windows.Forms.TextBox();
            this.Y_TempRez_T6 = new System.Windows.Forms.TextBox();
            this.X_TempRez_T6 = new System.Windows.Forms.TextBox();
            this.SizeFont_Temp_T6 = new System.Windows.Forms.TextBox();
            this.Font_Temp_T6 = new System.Windows.Forms.TextBox();
            this.H_Temp_T6 = new System.Windows.Forms.TextBox();
            this.W_Temp_T6 = new System.Windows.Forms.TextBox();
            this.Y_Temp_T6 = new System.Windows.Forms.TextBox();
            this.X_Temp_T6 = new System.Windows.Forms.TextBox();
            this.Add_Temp = new System.Windows.Forms.CheckBox();
            this.radioButton2 = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.radioButton3 = new System.Windows.Forms.CheckBox();
            this.radioButton6 = new System.Windows.Forms.CheckBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.radioButton7 = new System.Windows.Forms.CheckBox();
            this.SizeFont_Calendar_T6 = new System.Windows.Forms.TextBox();
            this.Font_Calendar_T6 = new System.Windows.Forms.TextBox();
            this.H_Calendar_T6 = new System.Windows.Forms.TextBox();
            this.W_Calendar_T6 = new System.Windows.Forms.TextBox();
            this.Y_Calendar_T6 = new System.Windows.Forms.TextBox();
            this.X_Calendar_T6 = new System.Windows.Forms.TextBox();
            this.Add_Calendar = new System.Windows.Forms.CheckBox();
            this.radioButton1 = new System.Windows.Forms.CheckBox();
            this.IP6_TEXT = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ID6_TEXT = new System.Windows.Forms.TextBox();
            this.label_IP = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PORT6_TEXT = new System.Windows.Forms.TextBox();
            this.SizeFont_Clock_T6 = new System.Windows.Forms.TextBox();
            this.Font_Clock_T6 = new System.Windows.Forms.TextBox();
            this.H_Clock_T6 = new System.Windows.Forms.TextBox();
            this.W_Clock_T6 = new System.Windows.Forms.TextBox();
            this.Y_Clock_T6 = new System.Windows.Forms.TextBox();
            this.X_Clock_T6 = new System.Windows.Forms.TextBox();
            this.Add_Clock = new System.Windows.Forms.CheckBox();
            this.CLOCK = new System.Windows.Forms.GroupBox();
            this.SC_BOX = new System.Windows.Forms.TextBox();
            this.FC_BOX = new System.Windows.Forms.TextBox();
            this.HC_BOX = new System.Windows.Forms.TextBox();
            this.WC_BOX = new System.Windows.Forms.TextBox();
            this.YC_BOX = new System.Windows.Forms.TextBox();
            this.XC_BOX = new System.Windows.Forms.TextBox();
            this.CLOCK_CHECK = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TIMER_BOX = new System.Windows.Forms.TextBox();
            this.PV_BOX = new System.Windows.Forms.TextBox();
            this.COM_BOX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.H_BOX = new System.Windows.Forms.TextBox();
            this.SV_BOX = new System.Windows.Forms.TextBox();
            this.W_BOX = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.IP_BOX = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ID_BOX = new System.Windows.Forms.TextBox();
            this.TV_BOX = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.PORT_BOX = new System.Windows.Forms.TextBox();
            this.ComLable = new System.Windows.Forms.Label();
            this.HV_BOX = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SK_BOX = new System.Windows.Forms.TextBox();
            this.FK_BOX = new System.Windows.Forms.TextBox();
            this.HK_BOX = new System.Windows.Forms.TextBox();
            this.WK_BOX = new System.Windows.Forms.TextBox();
            this.YK_BOX = new System.Windows.Forms.TextBox();
            this.XK_BOX = new System.Windows.Forms.TextBox();
            this.K_CHECK = new System.Windows.Forms.CheckBox();
            this.SPM_BOX = new System.Windows.Forms.TextBox();
            this.FPM_BOX = new System.Windows.Forms.TextBox();
            this.HPM_BOX = new System.Windows.Forms.TextBox();
            this.WPM_BOX = new System.Windows.Forms.TextBox();
            this.YPM_BOX = new System.Windows.Forms.TextBox();
            this.XPM_BOX = new System.Windows.Forms.TextBox();
            this.SP_BOX = new System.Windows.Forms.TextBox();
            this.FP_BOX = new System.Windows.Forms.TextBox();
            this.HP_BOX = new System.Windows.Forms.TextBox();
            this.WP_BOX = new System.Windows.Forms.TextBox();
            this.YP_BOX = new System.Windows.Forms.TextBox();
            this.XP_BOX = new System.Windows.Forms.TextBox();
            this.PRES_CHECK = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.DW_CHECK = new System.Windows.Forms.CheckBox();
            this.TEXTDW_BOX = new System.Windows.Forms.TextBox();
            this.SDW_BOX = new System.Windows.Forms.TextBox();
            this.FDW_BOX = new System.Windows.Forms.TextBox();
            this.HDWM_BOX = new System.Windows.Forms.TextBox();
            this.WDWM_BOX = new System.Windows.Forms.TextBox();
            this.YDWM_BOX = new System.Windows.Forms.TextBox();
            this.XDWM_BOX = new System.Windows.Forms.TextBox();
            this.DS_CHECK = new System.Windows.Forms.CheckBox();
            this.TEXTDS_BOX = new System.Windows.Forms.TextBox();
            this.SDS_BOX = new System.Windows.Forms.TextBox();
            this.FDS_BOX = new System.Windows.Forms.TextBox();
            this.HDSM_BOX = new System.Windows.Forms.TextBox();
            this.WDSM_BOX = new System.Windows.Forms.TextBox();
            this.YDSM_BOX = new System.Windows.Forms.TextBox();
            this.XDSM_BOX = new System.Windows.Forms.TextBox();
            this.DP_CHECK = new System.Windows.Forms.CheckBox();
            this.TEXTDP_BOX = new System.Windows.Forms.TextBox();
            this.SDP_BOX = new System.Windows.Forms.TextBox();
            this.FDP_BOX = new System.Windows.Forms.TextBox();
            this.HDPM_BOX = new System.Windows.Forms.TextBox();
            this.WDPM_BOX = new System.Windows.Forms.TextBox();
            this.YDPM_BOX = new System.Windows.Forms.TextBox();
            this.XDPM_BOX = new System.Windows.Forms.TextBox();
            this.DT_CHECK = new System.Windows.Forms.CheckBox();
            this.TEXTDT_BOX = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.SSM_BOX = new System.Windows.Forms.TextBox();
            this.FSM_BOX = new System.Windows.Forms.TextBox();
            this.HSM_BOX = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SWM_BOX = new System.Windows.Forms.TextBox();
            this.SDT_BOX = new System.Windows.Forms.TextBox();
            this.FWM_BOX = new System.Windows.Forms.TextBox();
            this.WSM_BOX = new System.Windows.Forms.TextBox();
            this.HWM_BOX = new System.Windows.Forms.TextBox();
            this.WWM_BOX = new System.Windows.Forms.TextBox();
            this.YSM_BOX = new System.Windows.Forms.TextBox();
            this.YWM_BOX = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.XWM_BOX = new System.Windows.Forms.TextBox();
            this.XSM_BOX = new System.Windows.Forms.TextBox();
            this.SW_BOX = new System.Windows.Forms.TextBox();
            this.SS_BOX = new System.Windows.Forms.TextBox();
            this.FW_BOX = new System.Windows.Forms.TextBox();
            this.FDT_BOX = new System.Windows.Forms.TextBox();
            this.HW_BOX = new System.Windows.Forms.TextBox();
            this.FS_BOX = new System.Windows.Forms.TextBox();
            this.WW_BOX = new System.Windows.Forms.TextBox();
            this.YW_BOX = new System.Windows.Forms.TextBox();
            this.HS_BOX = new System.Windows.Forms.TextBox();
            this.XW_BOX = new System.Windows.Forms.TextBox();
            this.STM_BOX = new System.Windows.Forms.TextBox();
            this.W_CHECK = new System.Windows.Forms.CheckBox();
            this.WS_BOX = new System.Windows.Forms.TextBox();
            this.YS_BOX = new System.Windows.Forms.TextBox();
            this.HDTM_BOX = new System.Windows.Forms.TextBox();
            this.XS_BOX = new System.Windows.Forms.TextBox();
            this.S_CHECK = new System.Windows.Forms.CheckBox();
            this.FTM_BOX = new System.Windows.Forms.TextBox();
            this.WDTM_BOX = new System.Windows.Forms.TextBox();
            this.HTM_BOX = new System.Windows.Forms.TextBox();
            this.YDTM_BOX = new System.Windows.Forms.TextBox();
            this.WTM_BOX = new System.Windows.Forms.TextBox();
            this.XDTM_BOX = new System.Windows.Forms.TextBox();
            this.YTM_BOX = new System.Windows.Forms.TextBox();
            this.XTM_BOX = new System.Windows.Forms.TextBox();
            this.ST_BOX = new System.Windows.Forms.TextBox();
            this.FT_BOX = new System.Windows.Forms.TextBox();
            this.HT_BOX = new System.Windows.Forms.TextBox();
            this.WT_BOX = new System.Windows.Forms.TextBox();
            this.YT_BOX = new System.Windows.Forms.TextBox();
            this.XT_BOX = new System.Windows.Forms.TextBox();
            this.TEMP_CHECK = new System.Windows.Forms.CheckBox();
            this.Start = new System.Windows.Forms.Button();
            this.Message = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ADRES_SET = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.DinamicT_6 = new System.Windows.Forms.CheckBox();
            this.DinamicT_1 = new System.Windows.Forms.CheckBox();
            this.DinamicT_5 = new System.Windows.Forms.CheckBox();
            this.DinamicT_2 = new System.Windows.Forms.CheckBox();
            this.DinamicT_4 = new System.Windows.Forms.CheckBox();
            this.DinamicT_3 = new System.Windows.Forms.CheckBox();
            this.StartDinamicA = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.SD_BOX = new System.Windows.Forms.TextBox();
            this.FD_BOX = new System.Windows.Forms.TextBox();
            this.HD_BOX = new System.Windows.Forms.TextBox();
            this.WD_BOX = new System.Windows.Forms.TextBox();
            this.YD_BOX = new System.Windows.Forms.TextBox();
            this.XD_BOX = new System.Windows.Forms.TextBox();
            this.D_CHECK = new System.Windows.Forms.CheckBox();
            this.Send_Info = new System.Windows.Forms.Button();
            this.DellDinamicA = new System.Windows.Forms.Button();
            this.Save_data = new System.Windows.Forms.Button();
            this.CLOCK.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort
            // 
            this.serialPort.BaudRate = 19200;
            this.serialPort.PortName = "COM3";
            this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort_DataReceived);
            // 
            // WRez
            // 
            this.WRez.AutoSize = true;
            this.WRez.Location = new System.Drawing.Point(205, 24);
            this.WRez.Name = "WRez";
            this.WRez.Size = new System.Drawing.Size(73, 17);
            this.WRez.TabIndex = 30;
            this.WRez.Text = "Значение";
            this.WRez.UseVisualStyleBackColor = true;
            // 
            // SizeFont_WRez_T6
            // 
            this.SizeFont_WRez_T6.Location = new System.Drawing.Point(307, 64);
            this.SizeFont_WRez_T6.Name = "SizeFont_WRez_T6";
            this.SizeFont_WRez_T6.Size = new System.Drawing.Size(28, 20);
            this.SizeFont_WRez_T6.TabIndex = 29;
            // 
            // Font_WRez_T6
            // 
            this.Font_WRez_T6.Location = new System.Drawing.Point(205, 64);
            this.Font_WRez_T6.Name = "Font_WRez_T6";
            this.Font_WRez_T6.Size = new System.Drawing.Size(96, 20);
            this.Font_WRez_T6.TabIndex = 28;
            // 
            // H_WRez_T6
            // 
            this.H_WRez_T6.Location = new System.Drawing.Point(307, 111);
            this.H_WRez_T6.Name = "H_WRez_T6";
            this.H_WRez_T6.Size = new System.Drawing.Size(28, 20);
            this.H_WRez_T6.TabIndex = 27;
            // 
            // W_WRez_T6
            // 
            this.W_WRez_T6.Location = new System.Drawing.Point(273, 111);
            this.W_WRez_T6.Name = "W_WRez_T6";
            this.W_WRez_T6.Size = new System.Drawing.Size(28, 20);
            this.W_WRez_T6.TabIndex = 26;
            // 
            // Y_WRez_T6
            // 
            this.Y_WRez_T6.Location = new System.Drawing.Point(239, 111);
            this.Y_WRez_T6.Name = "Y_WRez_T6";
            this.Y_WRez_T6.Size = new System.Drawing.Size(28, 20);
            this.Y_WRez_T6.TabIndex = 25;
            // 
            // X_WRez_T6
            // 
            this.X_WRez_T6.Location = new System.Drawing.Point(205, 111);
            this.X_WRez_T6.Name = "X_WRez_T6";
            this.X_WRez_T6.Size = new System.Drawing.Size(28, 20);
            this.X_WRez_T6.TabIndex = 24;
            // 
            // SizeFont_W_T6
            // 
            this.SizeFont_W_T6.Location = new System.Drawing.Point(129, 64);
            this.SizeFont_W_T6.Name = "SizeFont_W_T6";
            this.SizeFont_W_T6.Size = new System.Drawing.Size(28, 20);
            this.SizeFont_W_T6.TabIndex = 23;
            // 
            // Font_W_T6
            // 
            this.Font_W_T6.Location = new System.Drawing.Point(27, 64);
            this.Font_W_T6.Name = "Font_W_T6";
            this.Font_W_T6.Size = new System.Drawing.Size(96, 20);
            this.Font_W_T6.TabIndex = 22;
            // 
            // H_W_T6
            // 
            this.H_W_T6.Location = new System.Drawing.Point(129, 111);
            this.H_W_T6.Name = "H_W_T6";
            this.H_W_T6.Size = new System.Drawing.Size(28, 20);
            this.H_W_T6.TabIndex = 4;
            // 
            // W_W_T6
            // 
            this.W_W_T6.Location = new System.Drawing.Point(95, 111);
            this.W_W_T6.Name = "W_W_T6";
            this.W_W_T6.Size = new System.Drawing.Size(28, 20);
            this.W_W_T6.TabIndex = 3;
            // 
            // Y_W_T6
            // 
            this.Y_W_T6.Location = new System.Drawing.Point(61, 111);
            this.Y_W_T6.Name = "Y_W_T6";
            this.Y_W_T6.Size = new System.Drawing.Size(28, 20);
            this.Y_W_T6.TabIndex = 2;
            // 
            // X_W_T6
            // 
            this.X_W_T6.Location = new System.Drawing.Point(27, 111);
            this.X_W_T6.Name = "X_W_T6";
            this.X_W_T6.Size = new System.Drawing.Size(28, 20);
            this.X_W_T6.TabIndex = 1;
            // 
            // Add_W
            // 
            this.Add_W.AutoSize = true;
            this.Add_W.Location = new System.Drawing.Point(29, 24);
            this.Add_W.Name = "Add_W";
            this.Add_W.Size = new System.Drawing.Size(81, 17);
            this.Add_W.TabIndex = 0;
            this.Add_W.Text = "Влажность";
            this.Add_W.UseVisualStyleBackColor = true;
            // 
            // TempRez_T6
            // 
            this.TempRez_T6.AutoSize = true;
            this.TempRez_T6.Location = new System.Drawing.Point(205, 23);
            this.TempRez_T6.Name = "TempRez_T6";
            this.TempRez_T6.Size = new System.Drawing.Size(73, 17);
            this.TempRez_T6.TabIndex = 30;
            this.TempRez_T6.Text = "Значение";
            this.TempRez_T6.UseVisualStyleBackColor = true;
            // 
            // SizeFont_TempRez_T6
            // 
            this.SizeFont_TempRez_T6.Location = new System.Drawing.Point(307, 64);
            this.SizeFont_TempRez_T6.Name = "SizeFont_TempRez_T6";
            this.SizeFont_TempRez_T6.Size = new System.Drawing.Size(28, 20);
            this.SizeFont_TempRez_T6.TabIndex = 29;
            // 
            // Font_TempRez_T6
            // 
            this.Font_TempRez_T6.Location = new System.Drawing.Point(205, 64);
            this.Font_TempRez_T6.Name = "Font_TempRez_T6";
            this.Font_TempRez_T6.Size = new System.Drawing.Size(96, 20);
            this.Font_TempRez_T6.TabIndex = 28;
            // 
            // H_TempRez_T6
            // 
            this.H_TempRez_T6.Location = new System.Drawing.Point(307, 111);
            this.H_TempRez_T6.Name = "H_TempRez_T6";
            this.H_TempRez_T6.Size = new System.Drawing.Size(28, 20);
            this.H_TempRez_T6.TabIndex = 27;
            // 
            // W_TempRez_T6
            // 
            this.W_TempRez_T6.Location = new System.Drawing.Point(273, 111);
            this.W_TempRez_T6.Name = "W_TempRez_T6";
            this.W_TempRez_T6.Size = new System.Drawing.Size(28, 20);
            this.W_TempRez_T6.TabIndex = 26;
            // 
            // Y_TempRez_T6
            // 
            this.Y_TempRez_T6.Location = new System.Drawing.Point(239, 111);
            this.Y_TempRez_T6.Name = "Y_TempRez_T6";
            this.Y_TempRez_T6.Size = new System.Drawing.Size(28, 20);
            this.Y_TempRez_T6.TabIndex = 25;
            // 
            // X_TempRez_T6
            // 
            this.X_TempRez_T6.Location = new System.Drawing.Point(205, 111);
            this.X_TempRez_T6.Name = "X_TempRez_T6";
            this.X_TempRez_T6.Size = new System.Drawing.Size(28, 20);
            this.X_TempRez_T6.TabIndex = 24;
            // 
            // SizeFont_Temp_T6
            // 
            this.SizeFont_Temp_T6.Location = new System.Drawing.Point(129, 64);
            this.SizeFont_Temp_T6.Name = "SizeFont_Temp_T6";
            this.SizeFont_Temp_T6.Size = new System.Drawing.Size(28, 20);
            this.SizeFont_Temp_T6.TabIndex = 23;
            // 
            // Font_Temp_T6
            // 
            this.Font_Temp_T6.Location = new System.Drawing.Point(27, 64);
            this.Font_Temp_T6.Name = "Font_Temp_T6";
            this.Font_Temp_T6.Size = new System.Drawing.Size(96, 20);
            this.Font_Temp_T6.TabIndex = 22;
            // 
            // H_Temp_T6
            // 
            this.H_Temp_T6.Location = new System.Drawing.Point(129, 111);
            this.H_Temp_T6.Name = "H_Temp_T6";
            this.H_Temp_T6.Size = new System.Drawing.Size(28, 20);
            this.H_Temp_T6.TabIndex = 4;
            // 
            // W_Temp_T6
            // 
            this.W_Temp_T6.Location = new System.Drawing.Point(95, 111);
            this.W_Temp_T6.Name = "W_Temp_T6";
            this.W_Temp_T6.Size = new System.Drawing.Size(28, 20);
            this.W_Temp_T6.TabIndex = 3;
            // 
            // Y_Temp_T6
            // 
            this.Y_Temp_T6.Location = new System.Drawing.Point(61, 111);
            this.Y_Temp_T6.Name = "Y_Temp_T6";
            this.Y_Temp_T6.Size = new System.Drawing.Size(28, 20);
            this.Y_Temp_T6.TabIndex = 2;
            // 
            // X_Temp_T6
            // 
            this.X_Temp_T6.Location = new System.Drawing.Point(27, 111);
            this.X_Temp_T6.Name = "X_Temp_T6";
            this.X_Temp_T6.Size = new System.Drawing.Size(28, 20);
            this.X_Temp_T6.TabIndex = 1;
            // 
            // Add_Temp
            // 
            this.Add_Temp.AutoSize = true;
            this.Add_Temp.Location = new System.Drawing.Point(29, 24);
            this.Add_Temp.Name = "Add_Temp";
            this.Add_Temp.Size = new System.Drawing.Size(92, 17);
            this.Add_Temp.TabIndex = 0;
            this.Add_Temp.Text = "Температура";
            this.Add_Temp.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(205, 24);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(73, 17);
            this.radioButton2.TabIndex = 30;
            this.radioButton2.Text = "Значение";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(307, 64);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(28, 20);
            this.textBox1.TabIndex = 29;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(205, 64);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(96, 20);
            this.textBox2.TabIndex = 28;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(307, 111);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(28, 20);
            this.textBox3.TabIndex = 27;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(273, 111);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(28, 20);
            this.textBox4.TabIndex = 26;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(239, 111);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(28, 20);
            this.textBox5.TabIndex = 25;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(205, 111);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(28, 20);
            this.textBox6.TabIndex = 24;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(129, 64);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(28, 20);
            this.textBox7.TabIndex = 23;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(27, 64);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(96, 20);
            this.textBox8.TabIndex = 22;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(129, 111);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(28, 20);
            this.textBox9.TabIndex = 4;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(95, 111);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(28, 20);
            this.textBox10.TabIndex = 3;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(61, 111);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(28, 20);
            this.textBox11.TabIndex = 2;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(27, 111);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(28, 20);
            this.textBox12.TabIndex = 1;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(29, 24);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(105, 17);
            this.radioButton3.TabIndex = 0;
            this.radioButton3.Text = "Скорость ветра";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(205, 24);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(73, 17);
            this.radioButton6.TabIndex = 30;
            this.radioButton6.Text = "Значение";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(307, 64);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(28, 20);
            this.textBox25.TabIndex = 29;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(205, 64);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(96, 20);
            this.textBox26.TabIndex = 28;
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(307, 111);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(28, 20);
            this.textBox27.TabIndex = 27;
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(273, 111);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(28, 20);
            this.textBox28.TabIndex = 26;
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(239, 111);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(28, 20);
            this.textBox29.TabIndex = 25;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(205, 111);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(28, 20);
            this.textBox30.TabIndex = 24;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(129, 64);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(28, 20);
            this.textBox31.TabIndex = 23;
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(27, 64);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(96, 20);
            this.textBox32.TabIndex = 22;
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(129, 111);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(28, 20);
            this.textBox33.TabIndex = 4;
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(95, 111);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(28, 20);
            this.textBox34.TabIndex = 3;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(61, 111);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(28, 20);
            this.textBox35.TabIndex = 2;
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(27, 111);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(28, 20);
            this.textBox36.TabIndex = 1;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(29, 24);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(76, 17);
            this.radioButton7.TabIndex = 0;
            this.radioButton7.Text = "Давление";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // SizeFont_Calendar_T6
            // 
            this.SizeFont_Calendar_T6.Location = new System.Drawing.Point(121, 64);
            this.SizeFont_Calendar_T6.Name = "SizeFont_Calendar_T6";
            this.SizeFont_Calendar_T6.Size = new System.Drawing.Size(28, 20);
            this.SizeFont_Calendar_T6.TabIndex = 23;
            // 
            // Font_Calendar_T6
            // 
            this.Font_Calendar_T6.Location = new System.Drawing.Point(19, 64);
            this.Font_Calendar_T6.Name = "Font_Calendar_T6";
            this.Font_Calendar_T6.Size = new System.Drawing.Size(96, 20);
            this.Font_Calendar_T6.TabIndex = 22;
            // 
            // H_Calendar_T6
            // 
            this.H_Calendar_T6.Location = new System.Drawing.Point(121, 111);
            this.H_Calendar_T6.Name = "H_Calendar_T6";
            this.H_Calendar_T6.Size = new System.Drawing.Size(28, 20);
            this.H_Calendar_T6.TabIndex = 4;
            // 
            // W_Calendar_T6
            // 
            this.W_Calendar_T6.Location = new System.Drawing.Point(87, 111);
            this.W_Calendar_T6.Name = "W_Calendar_T6";
            this.W_Calendar_T6.Size = new System.Drawing.Size(28, 20);
            this.W_Calendar_T6.TabIndex = 3;
            // 
            // Y_Calendar_T6
            // 
            this.Y_Calendar_T6.Location = new System.Drawing.Point(53, 111);
            this.Y_Calendar_T6.Name = "Y_Calendar_T6";
            this.Y_Calendar_T6.Size = new System.Drawing.Size(28, 20);
            this.Y_Calendar_T6.TabIndex = 2;
            // 
            // X_Calendar_T6
            // 
            this.X_Calendar_T6.Location = new System.Drawing.Point(19, 111);
            this.X_Calendar_T6.Name = "X_Calendar_T6";
            this.X_Calendar_T6.Size = new System.Drawing.Size(28, 20);
            this.X_Calendar_T6.TabIndex = 1;
            // 
            // Add_Calendar
            // 
            this.Add_Calendar.AutoSize = true;
            this.Add_Calendar.Location = new System.Drawing.Point(21, 25);
            this.Add_Calendar.Name = "Add_Calendar";
            this.Add_Calendar.Size = new System.Drawing.Size(80, 17);
            this.Add_Calendar.TabIndex = 0;
            this.Add_Calendar.Text = "Календарь";
            this.Add_Calendar.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(4, 17);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(91, 17);
            this.radioButton1.TabIndex = 23;
            this.radioButton1.Text = "Адрес в сети";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // IP6_TEXT
            // 
            this.IP6_TEXT.Location = new System.Drawing.Point(142, 15);
            this.IP6_TEXT.Name = "IP6_TEXT";
            this.IP6_TEXT.Size = new System.Drawing.Size(72, 20);
            this.IP6_TEXT.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(311, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "ID";
            // 
            // ID6_TEXT
            // 
            this.ID6_TEXT.Location = new System.Drawing.Point(337, 13);
            this.ID6_TEXT.Name = "ID6_TEXT";
            this.ID6_TEXT.Size = new System.Drawing.Size(15, 20);
            this.ID6_TEXT.TabIndex = 5;
            // 
            // label_IP
            // 
            this.label_IP.AutoSize = true;
            this.label_IP.Location = new System.Drawing.Point(118, 20);
            this.label_IP.Name = "label_IP";
            this.label_IP.Size = new System.Drawing.Size(17, 13);
            this.label_IP.TabIndex = 2;
            this.label_IP.Text = "IP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(220, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "PORT";
            // 
            // PORT6_TEXT
            // 
            this.PORT6_TEXT.Location = new System.Drawing.Point(270, 14);
            this.PORT6_TEXT.Name = "PORT6_TEXT";
            this.PORT6_TEXT.Size = new System.Drawing.Size(35, 20);
            this.PORT6_TEXT.TabIndex = 3;
            // 
            // SizeFont_Clock_T6
            // 
            this.SizeFont_Clock_T6.Location = new System.Drawing.Point(123, 64);
            this.SizeFont_Clock_T6.Name = "SizeFont_Clock_T6";
            this.SizeFont_Clock_T6.Size = new System.Drawing.Size(28, 20);
            this.SizeFont_Clock_T6.TabIndex = 23;
            // 
            // Font_Clock_T6
            // 
            this.Font_Clock_T6.Location = new System.Drawing.Point(21, 64);
            this.Font_Clock_T6.Name = "Font_Clock_T6";
            this.Font_Clock_T6.Size = new System.Drawing.Size(96, 20);
            this.Font_Clock_T6.TabIndex = 22;
            // 
            // H_Clock_T6
            // 
            this.H_Clock_T6.Location = new System.Drawing.Point(123, 111);
            this.H_Clock_T6.Name = "H_Clock_T6";
            this.H_Clock_T6.Size = new System.Drawing.Size(28, 20);
            this.H_Clock_T6.TabIndex = 4;
            // 
            // W_Clock_T6
            // 
            this.W_Clock_T6.Location = new System.Drawing.Point(89, 111);
            this.W_Clock_T6.Name = "W_Clock_T6";
            this.W_Clock_T6.Size = new System.Drawing.Size(28, 20);
            this.W_Clock_T6.TabIndex = 3;
            // 
            // Y_Clock_T6
            // 
            this.Y_Clock_T6.Location = new System.Drawing.Point(55, 111);
            this.Y_Clock_T6.Name = "Y_Clock_T6";
            this.Y_Clock_T6.Size = new System.Drawing.Size(28, 20);
            this.Y_Clock_T6.TabIndex = 2;
            // 
            // X_Clock_T6
            // 
            this.X_Clock_T6.Location = new System.Drawing.Point(21, 111);
            this.X_Clock_T6.Name = "X_Clock_T6";
            this.X_Clock_T6.Size = new System.Drawing.Size(28, 20);
            this.X_Clock_T6.TabIndex = 1;
            // 
            // Add_Clock
            // 
            this.Add_Clock.AutoSize = true;
            this.Add_Clock.Location = new System.Drawing.Point(23, 24);
            this.Add_Clock.Name = "Add_Clock";
            this.Add_Clock.Size = new System.Drawing.Size(53, 17);
            this.Add_Clock.TabIndex = 0;
            this.Add_Clock.Text = "Часы";
            this.Add_Clock.UseVisualStyleBackColor = true;
            // 
            // CLOCK
            // 
            this.CLOCK.Controls.Add(this.SC_BOX);
            this.CLOCK.Controls.Add(this.FC_BOX);
            this.CLOCK.Controls.Add(this.HC_BOX);
            this.CLOCK.Controls.Add(this.WC_BOX);
            this.CLOCK.Controls.Add(this.YC_BOX);
            this.CLOCK.Controls.Add(this.XC_BOX);
            this.CLOCK.Controls.Add(this.CLOCK_CHECK);
            this.CLOCK.Location = new System.Drawing.Point(13, 204);
            this.CLOCK.Name = "CLOCK";
            this.CLOCK.Size = new System.Drawing.Size(167, 140);
            this.CLOCK.TabIndex = 30;
            this.CLOCK.TabStop = false;
            this.CLOCK.Enter += new System.EventHandler(this.CLOCK_Enter_1);
            // 
            // SC_BOX
            // 
            this.SC_BOX.Location = new System.Drawing.Point(123, 64);
            this.SC_BOX.Name = "SC_BOX";
            this.SC_BOX.Size = new System.Drawing.Size(28, 20);
            this.SC_BOX.TabIndex = 23;
            // 
            // FC_BOX
            // 
            this.FC_BOX.Location = new System.Drawing.Point(21, 64);
            this.FC_BOX.Name = "FC_BOX";
            this.FC_BOX.Size = new System.Drawing.Size(96, 20);
            this.FC_BOX.TabIndex = 22;
            this.FC_BOX.TextChanged += new System.EventHandler(this.textBox83_TextChanged);
            // 
            // HC_BOX
            // 
            this.HC_BOX.Location = new System.Drawing.Point(123, 111);
            this.HC_BOX.Name = "HC_BOX";
            this.HC_BOX.Size = new System.Drawing.Size(28, 20);
            this.HC_BOX.TabIndex = 4;
            this.HC_BOX.TextChanged += new System.EventHandler(this.HC_BOX_TextChanged);
            // 
            // WC_BOX
            // 
            this.WC_BOX.Location = new System.Drawing.Point(89, 111);
            this.WC_BOX.Name = "WC_BOX";
            this.WC_BOX.Size = new System.Drawing.Size(28, 20);
            this.WC_BOX.TabIndex = 3;
            // 
            // YC_BOX
            // 
            this.YC_BOX.Location = new System.Drawing.Point(55, 111);
            this.YC_BOX.Name = "YC_BOX";
            this.YC_BOX.Size = new System.Drawing.Size(28, 20);
            this.YC_BOX.TabIndex = 2;
            this.YC_BOX.TextChanged += new System.EventHandler(this.YC_BOX_TextChanged);
            // 
            // XC_BOX
            // 
            this.XC_BOX.Location = new System.Drawing.Point(21, 111);
            this.XC_BOX.Name = "XC_BOX";
            this.XC_BOX.Size = new System.Drawing.Size(28, 20);
            this.XC_BOX.TabIndex = 1;
            this.XC_BOX.TextChanged += new System.EventHandler(this.XC_BOX_TextChanged);
            // 
            // CLOCK_CHECK
            // 
            this.CLOCK_CHECK.AutoSize = true;
            this.CLOCK_CHECK.Location = new System.Drawing.Point(23, 24);
            this.CLOCK_CHECK.Name = "CLOCK_CHECK";
            this.CLOCK_CHECK.Size = new System.Drawing.Size(54, 17);
            this.CLOCK_CHECK.TabIndex = 0;
            this.CLOCK_CHECK.Text = "Часы";
            this.CLOCK_CHECK.UseVisualStyleBackColor = true;
            this.CLOCK_CHECK.CheckedChanged += new System.EventHandler(this.CLOCK_CHECK_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.TIMER_BOX);
            this.groupBox2.Controls.Add(this.PV_BOX);
            this.groupBox2.Controls.Add(this.COM_BOX);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.H_BOX);
            this.groupBox2.Controls.Add(this.SV_BOX);
            this.groupBox2.Controls.Add(this.W_BOX);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.IP_BOX);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.ID_BOX);
            this.groupBox2.Controls.Add(this.TV_BOX);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.PORT_BOX);
            this.groupBox2.Controls.Add(this.ComLable);
            this.groupBox2.Controls.Add(this.HV_BOX);
            this.groupBox2.Location = new System.Drawing.Point(13, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(907, 48);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter_1);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(527, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 13);
            this.label17.TabIndex = 35;
            this.label17.Text = "Timer";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(276, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 13);
            this.label16.TabIndex = 33;
            this.label16.Text = "H";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(218, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "W";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // TIMER_BOX
            // 
            this.TIMER_BOX.Location = new System.Drawing.Point(565, 16);
            this.TIMER_BOX.Name = "TIMER_BOX";
            this.TIMER_BOX.Size = new System.Drawing.Size(31, 20);
            this.TIMER_BOX.TabIndex = 34;
            this.TIMER_BOX.TextChanged += new System.EventHandler(this.TIMER_BOX_TextChanged);
            // 
            // PV_BOX
            // 
            this.PV_BOX.Location = new System.Drawing.Point(790, 16);
            this.PV_BOX.Name = "PV_BOX";
            this.PV_BOX.Size = new System.Drawing.Size(31, 20);
            this.PV_BOX.TabIndex = 2;
            this.PV_BOX.TextChanged += new System.EventHandler(this.TextPres_TextChanged);
            // 
            // COM_BOX
            // 
            this.COM_BOX.Location = new System.Drawing.Point(482, 16);
            this.COM_BOX.Name = "COM_BOX";
            this.COM_BOX.Size = new System.Drawing.Size(42, 20);
            this.COM_BOX.TabIndex = 25;
            this.COM_BOX.TextChanged += new System.EventHandler(this.textBox89_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(600, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Т,  С";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // H_BOX
            // 
            this.H_BOX.Location = new System.Drawing.Point(296, 16);
            this.H_BOX.Name = "H_BOX";
            this.H_BOX.Size = new System.Drawing.Size(26, 20);
            this.H_BOX.TabIndex = 31;
            // 
            // SV_BOX
            // 
            this.SV_BOX.Location = new System.Drawing.Point(863, 16);
            this.SV_BOX.Name = "SV_BOX";
            this.SV_BOX.Size = new System.Drawing.Size(36, 20);
            this.SV_BOX.TabIndex = 29;
            // 
            // W_BOX
            // 
            this.W_BOX.Location = new System.Drawing.Point(242, 16);
            this.W_BOX.Name = "W_BOX";
            this.W_BOX.Size = new System.Drawing.Size(27, 20);
            this.W_BOX.TabIndex = 30;
            this.W_BOX.TextChanged += new System.EventHandler(this.W_BOX_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(683, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Н, %";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Табло №";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(824, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "S, м/с";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // IP_BOX
            // 
            this.IP_BOX.Location = new System.Drawing.Point(139, 16);
            this.IP_BOX.Name = "IP_BOX";
            this.IP_BOX.Size = new System.Drawing.Size(72, 20);
            this.IP_BOX.TabIndex = 1;
            this.IP_BOX.TextChanged += new System.EventHandler(this.IP_BOX_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(752, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Р, мм";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(404, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "ID";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // ID_BOX
            // 
            this.ID_BOX.Location = new System.Drawing.Point(427, 17);
            this.ID_BOX.Name = "ID_BOX";
            this.ID_BOX.Size = new System.Drawing.Size(15, 20);
            this.ID_BOX.TabIndex = 5;
            this.ID_BOX.TextChanged += new System.EventHandler(this.ID_BOX_TextChanged);
            // 
            // TV_BOX
            // 
            this.TV_BOX.Location = new System.Drawing.Point(634, 16);
            this.TV_BOX.Name = "TV_BOX";
            this.TV_BOX.Size = new System.Drawing.Size(41, 20);
            this.TV_BOX.TabIndex = 3;
            this.TV_BOX.TextChanged += new System.EventHandler(this.TextTemp_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(116, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "IP";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(326, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "PORT";
            // 
            // PORT_BOX
            // 
            this.PORT_BOX.Location = new System.Drawing.Point(366, 16);
            this.PORT_BOX.Name = "PORT_BOX";
            this.PORT_BOX.Size = new System.Drawing.Size(35, 20);
            this.PORT_BOX.TabIndex = 3;
            // 
            // ComLable
            // 
            this.ComLable.AutoSize = true;
            this.ComLable.Location = new System.Drawing.Point(447, 21);
            this.ComLable.Name = "ComLable";
            this.ComLable.Size = new System.Drawing.Size(31, 13);
            this.ComLable.TabIndex = 24;
            this.ComLable.Text = "COM";
            // 
            // HV_BOX
            // 
            this.HV_BOX.Location = new System.Drawing.Point(718, 16);
            this.HV_BOX.Name = "HV_BOX";
            this.HV_BOX.Size = new System.Drawing.Size(31, 20);
            this.HV_BOX.TabIndex = 4;
            this.HV_BOX.TextChanged += new System.EventHandler(this.TestHum_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.SK_BOX);
            this.groupBox3.Controls.Add(this.FK_BOX);
            this.groupBox3.Controls.Add(this.HK_BOX);
            this.groupBox3.Controls.Add(this.WK_BOX);
            this.groupBox3.Controls.Add(this.YK_BOX);
            this.groupBox3.Controls.Add(this.XK_BOX);
            this.groupBox3.Controls.Add(this.K_CHECK);
            this.groupBox3.Location = new System.Drawing.Point(14, 349);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(167, 140);
            this.groupBox3.TabIndex = 32;
            this.groupBox3.TabStop = false;
            // 
            // SK_BOX
            // 
            this.SK_BOX.Location = new System.Drawing.Point(121, 64);
            this.SK_BOX.Name = "SK_BOX";
            this.SK_BOX.Size = new System.Drawing.Size(28, 20);
            this.SK_BOX.TabIndex = 23;
            // 
            // FK_BOX
            // 
            this.FK_BOX.Location = new System.Drawing.Point(19, 64);
            this.FK_BOX.Name = "FK_BOX";
            this.FK_BOX.Size = new System.Drawing.Size(96, 20);
            this.FK_BOX.TabIndex = 22;
            // 
            // HK_BOX
            // 
            this.HK_BOX.Location = new System.Drawing.Point(121, 111);
            this.HK_BOX.Name = "HK_BOX";
            this.HK_BOX.Size = new System.Drawing.Size(28, 20);
            this.HK_BOX.TabIndex = 4;
            // 
            // WK_BOX
            // 
            this.WK_BOX.Location = new System.Drawing.Point(87, 111);
            this.WK_BOX.Name = "WK_BOX";
            this.WK_BOX.Size = new System.Drawing.Size(28, 20);
            this.WK_BOX.TabIndex = 3;
            this.WK_BOX.TextChanged += new System.EventHandler(this.WK_BOX_TextChanged);
            // 
            // YK_BOX
            // 
            this.YK_BOX.Location = new System.Drawing.Point(53, 111);
            this.YK_BOX.Name = "YK_BOX";
            this.YK_BOX.Size = new System.Drawing.Size(28, 20);
            this.YK_BOX.TabIndex = 2;
            // 
            // XK_BOX
            // 
            this.XK_BOX.Location = new System.Drawing.Point(19, 111);
            this.XK_BOX.Name = "XK_BOX";
            this.XK_BOX.Size = new System.Drawing.Size(28, 20);
            this.XK_BOX.TabIndex = 1;
            // 
            // K_CHECK
            // 
            this.K_CHECK.AutoSize = true;
            this.K_CHECK.Location = new System.Drawing.Point(21, 25);
            this.K_CHECK.Name = "K_CHECK";
            this.K_CHECK.Size = new System.Drawing.Size(81, 17);
            this.K_CHECK.TabIndex = 0;
            this.K_CHECK.Text = "Календарь";
            this.K_CHECK.UseVisualStyleBackColor = true;
            this.K_CHECK.CheckedChanged += new System.EventHandler(this.K_CHECK_CheckedChanged);
            // 
            // SPM_BOX
            // 
            this.SPM_BOX.Location = new System.Drawing.Point(125, 164);
            this.SPM_BOX.Name = "SPM_BOX";
            this.SPM_BOX.Size = new System.Drawing.Size(28, 20);
            this.SPM_BOX.TabIndex = 29;
            // 
            // FPM_BOX
            // 
            this.FPM_BOX.Location = new System.Drawing.Point(23, 164);
            this.FPM_BOX.Name = "FPM_BOX";
            this.FPM_BOX.Size = new System.Drawing.Size(96, 20);
            this.FPM_BOX.TabIndex = 28;
            // 
            // HPM_BOX
            // 
            this.HPM_BOX.Location = new System.Drawing.Point(125, 204);
            this.HPM_BOX.Name = "HPM_BOX";
            this.HPM_BOX.Size = new System.Drawing.Size(28, 20);
            this.HPM_BOX.TabIndex = 27;
            // 
            // WPM_BOX
            // 
            this.WPM_BOX.Location = new System.Drawing.Point(91, 204);
            this.WPM_BOX.Name = "WPM_BOX";
            this.WPM_BOX.Size = new System.Drawing.Size(28, 20);
            this.WPM_BOX.TabIndex = 26;
            // 
            // YPM_BOX
            // 
            this.YPM_BOX.Location = new System.Drawing.Point(57, 204);
            this.YPM_BOX.Name = "YPM_BOX";
            this.YPM_BOX.Size = new System.Drawing.Size(28, 20);
            this.YPM_BOX.TabIndex = 25;
            // 
            // XPM_BOX
            // 
            this.XPM_BOX.Location = new System.Drawing.Point(23, 204);
            this.XPM_BOX.Name = "XPM_BOX";
            this.XPM_BOX.Size = new System.Drawing.Size(28, 20);
            this.XPM_BOX.TabIndex = 24;
            this.XPM_BOX.TextChanged += new System.EventHandler(this.XPM_BOX_TextChanged);
            // 
            // SP_BOX
            // 
            this.SP_BOX.Location = new System.Drawing.Point(128, 62);
            this.SP_BOX.Name = "SP_BOX";
            this.SP_BOX.Size = new System.Drawing.Size(28, 20);
            this.SP_BOX.TabIndex = 23;
            // 
            // FP_BOX
            // 
            this.FP_BOX.Location = new System.Drawing.Point(26, 62);
            this.FP_BOX.Name = "FP_BOX";
            this.FP_BOX.Size = new System.Drawing.Size(96, 20);
            this.FP_BOX.TabIndex = 22;
            this.FP_BOX.TextChanged += new System.EventHandler(this.FP_BOX_TextChanged);
            // 
            // HP_BOX
            // 
            this.HP_BOX.Location = new System.Drawing.Point(126, 104);
            this.HP_BOX.Name = "HP_BOX";
            this.HP_BOX.Size = new System.Drawing.Size(28, 20);
            this.HP_BOX.TabIndex = 4;
            this.HP_BOX.TextChanged += new System.EventHandler(this.HP_BOX_TextChanged);
            // 
            // WP_BOX
            // 
            this.WP_BOX.Location = new System.Drawing.Point(92, 104);
            this.WP_BOX.Name = "WP_BOX";
            this.WP_BOX.Size = new System.Drawing.Size(28, 20);
            this.WP_BOX.TabIndex = 3;
            this.WP_BOX.TextChanged += new System.EventHandler(this.WP_BOX_TextChanged);
            // 
            // YP_BOX
            // 
            this.YP_BOX.Location = new System.Drawing.Point(58, 104);
            this.YP_BOX.Name = "YP_BOX";
            this.YP_BOX.Size = new System.Drawing.Size(28, 20);
            this.YP_BOX.TabIndex = 2;
            this.YP_BOX.TextChanged += new System.EventHandler(this.YP_BOX_TextChanged);
            // 
            // XP_BOX
            // 
            this.XP_BOX.Location = new System.Drawing.Point(24, 104);
            this.XP_BOX.Name = "XP_BOX";
            this.XP_BOX.Size = new System.Drawing.Size(28, 20);
            this.XP_BOX.TabIndex = 1;
            this.XP_BOX.TextChanged += new System.EventHandler(this.XP_BOX_TextChanged);
            // 
            // PRES_CHECK
            // 
            this.PRES_CHECK.AutoSize = true;
            this.PRES_CHECK.Location = new System.Drawing.Point(28, 22);
            this.PRES_CHECK.Name = "PRES_CHECK";
            this.PRES_CHECK.Size = new System.Drawing.Size(77, 17);
            this.PRES_CHECK.TabIndex = 0;
            this.PRES_CHECK.Text = "Давление";
            this.PRES_CHECK.UseVisualStyleBackColor = true;
            this.PRES_CHECK.CheckedChanged += new System.EventHandler(this.PRES_CHECK_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.DW_CHECK);
            this.groupBox5.Controls.Add(this.TEXTDW_BOX);
            this.groupBox5.Controls.Add(this.SDW_BOX);
            this.groupBox5.Controls.Add(this.FDW_BOX);
            this.groupBox5.Controls.Add(this.HDWM_BOX);
            this.groupBox5.Controls.Add(this.WDWM_BOX);
            this.groupBox5.Controls.Add(this.YDWM_BOX);
            this.groupBox5.Controls.Add(this.XDWM_BOX);
            this.groupBox5.Controls.Add(this.DS_CHECK);
            this.groupBox5.Controls.Add(this.TEXTDS_BOX);
            this.groupBox5.Controls.Add(this.SDS_BOX);
            this.groupBox5.Controls.Add(this.FDS_BOX);
            this.groupBox5.Controls.Add(this.HDSM_BOX);
            this.groupBox5.Controls.Add(this.WDSM_BOX);
            this.groupBox5.Controls.Add(this.YDSM_BOX);
            this.groupBox5.Controls.Add(this.XDSM_BOX);
            this.groupBox5.Controls.Add(this.DP_CHECK);
            this.groupBox5.Controls.Add(this.TEXTDP_BOX);
            this.groupBox5.Controls.Add(this.SDP_BOX);
            this.groupBox5.Controls.Add(this.FDP_BOX);
            this.groupBox5.Controls.Add(this.HDPM_BOX);
            this.groupBox5.Controls.Add(this.WDPM_BOX);
            this.groupBox5.Controls.Add(this.YDPM_BOX);
            this.groupBox5.Controls.Add(this.XDPM_BOX);
            this.groupBox5.Controls.Add(this.DT_CHECK);
            this.groupBox5.Controls.Add(this.TEXTDT_BOX);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.SSM_BOX);
            this.groupBox5.Controls.Add(this.FSM_BOX);
            this.groupBox5.Controls.Add(this.SPM_BOX);
            this.groupBox5.Controls.Add(this.HSM_BOX);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.SWM_BOX);
            this.groupBox5.Controls.Add(this.SDT_BOX);
            this.groupBox5.Controls.Add(this.FWM_BOX);
            this.groupBox5.Controls.Add(this.WSM_BOX);
            this.groupBox5.Controls.Add(this.HWM_BOX);
            this.groupBox5.Controls.Add(this.FPM_BOX);
            this.groupBox5.Controls.Add(this.WWM_BOX);
            this.groupBox5.Controls.Add(this.YSM_BOX);
            this.groupBox5.Controls.Add(this.YWM_BOX);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.XWM_BOX);
            this.groupBox5.Controls.Add(this.XSM_BOX);
            this.groupBox5.Controls.Add(this.HPM_BOX);
            this.groupBox5.Controls.Add(this.SW_BOX);
            this.groupBox5.Controls.Add(this.SS_BOX);
            this.groupBox5.Controls.Add(this.FW_BOX);
            this.groupBox5.Controls.Add(this.FDT_BOX);
            this.groupBox5.Controls.Add(this.HW_BOX);
            this.groupBox5.Controls.Add(this.FS_BOX);
            this.groupBox5.Controls.Add(this.WW_BOX);
            this.groupBox5.Controls.Add(this.WPM_BOX);
            this.groupBox5.Controls.Add(this.YW_BOX);
            this.groupBox5.Controls.Add(this.HS_BOX);
            this.groupBox5.Controls.Add(this.XW_BOX);
            this.groupBox5.Controls.Add(this.STM_BOX);
            this.groupBox5.Controls.Add(this.W_CHECK);
            this.groupBox5.Controls.Add(this.WS_BOX);
            this.groupBox5.Controls.Add(this.YPM_BOX);
            this.groupBox5.Controls.Add(this.YS_BOX);
            this.groupBox5.Controls.Add(this.HDTM_BOX);
            this.groupBox5.Controls.Add(this.XS_BOX);
            this.groupBox5.Controls.Add(this.XPM_BOX);
            this.groupBox5.Controls.Add(this.S_CHECK);
            this.groupBox5.Controls.Add(this.FTM_BOX);
            this.groupBox5.Controls.Add(this.SP_BOX);
            this.groupBox5.Controls.Add(this.WDTM_BOX);
            this.groupBox5.Controls.Add(this.FP_BOX);
            this.groupBox5.Controls.Add(this.HTM_BOX);
            this.groupBox5.Controls.Add(this.HP_BOX);
            this.groupBox5.Controls.Add(this.YDTM_BOX);
            this.groupBox5.Controls.Add(this.WP_BOX);
            this.groupBox5.Controls.Add(this.WTM_BOX);
            this.groupBox5.Controls.Add(this.YP_BOX);
            this.groupBox5.Controls.Add(this.XDTM_BOX);
            this.groupBox5.Controls.Add(this.XP_BOX);
            this.groupBox5.Controls.Add(this.YTM_BOX);
            this.groupBox5.Controls.Add(this.PRES_CHECK);
            this.groupBox5.Controls.Add(this.XTM_BOX);
            this.groupBox5.Controls.Add(this.ST_BOX);
            this.groupBox5.Controls.Add(this.FT_BOX);
            this.groupBox5.Controls.Add(this.HT_BOX);
            this.groupBox5.Controls.Add(this.WT_BOX);
            this.groupBox5.Controls.Add(this.YT_BOX);
            this.groupBox5.Controls.Add(this.XT_BOX);
            this.groupBox5.Controls.Add(this.TEMP_CHECK);
            this.groupBox5.Location = new System.Drawing.Point(184, 63);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(736, 379);
            this.groupBox5.TabIndex = 35;
            this.groupBox5.TabStop = false;
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // DW_CHECK
            // 
            this.DW_CHECK.AutoSize = true;
            this.DW_CHECK.Location = new System.Drawing.Point(397, 245);
            this.DW_CHECK.Name = "DW_CHECK";
            this.DW_CHECK.Size = new System.Drawing.Size(106, 17);
            this.DW_CHECK.TabIndex = 80;
            this.DW_CHECK.Text = "Дополнительно";
            this.DW_CHECK.UseVisualStyleBackColor = true;
            this.DW_CHECK.CheckedChanged += new System.EventHandler(this.DW_CHECK_CheckedChanged);
            // 
            // TEXTDW_BOX
            // 
            this.TEXTDW_BOX.Location = new System.Drawing.Point(397, 275);
            this.TEXTDW_BOX.Name = "TEXTDW_BOX";
            this.TEXTDW_BOX.Size = new System.Drawing.Size(130, 20);
            this.TEXTDW_BOX.TabIndex = 79;
            this.TEXTDW_BOX.TextChanged += new System.EventHandler(this.TEXTDW_BOX_TextChanged);
            // 
            // SDW_BOX
            // 
            this.SDW_BOX.Location = new System.Drawing.Point(499, 311);
            this.SDW_BOX.Name = "SDW_BOX";
            this.SDW_BOX.Size = new System.Drawing.Size(28, 20);
            this.SDW_BOX.TabIndex = 78;
            this.SDW_BOX.TextChanged += new System.EventHandler(this.SDW_BOX_TextChanged);
            // 
            // FDW_BOX
            // 
            this.FDW_BOX.Location = new System.Drawing.Point(397, 311);
            this.FDW_BOX.Name = "FDW_BOX";
            this.FDW_BOX.Size = new System.Drawing.Size(96, 20);
            this.FDW_BOX.TabIndex = 77;
            this.FDW_BOX.TextChanged += new System.EventHandler(this.FDW_BOX_TextChanged);
            // 
            // HDWM_BOX
            // 
            this.HDWM_BOX.Location = new System.Drawing.Point(499, 348);
            this.HDWM_BOX.Name = "HDWM_BOX";
            this.HDWM_BOX.Size = new System.Drawing.Size(28, 20);
            this.HDWM_BOX.TabIndex = 76;
            this.HDWM_BOX.TextChanged += new System.EventHandler(this.HDWM_BOX_TextChanged);
            // 
            // WDWM_BOX
            // 
            this.WDWM_BOX.Location = new System.Drawing.Point(465, 348);
            this.WDWM_BOX.Name = "WDWM_BOX";
            this.WDWM_BOX.Size = new System.Drawing.Size(28, 20);
            this.WDWM_BOX.TabIndex = 75;
            // 
            // YDWM_BOX
            // 
            this.YDWM_BOX.Location = new System.Drawing.Point(431, 348);
            this.YDWM_BOX.Name = "YDWM_BOX";
            this.YDWM_BOX.Size = new System.Drawing.Size(28, 20);
            this.YDWM_BOX.TabIndex = 74;
            this.YDWM_BOX.TextChanged += new System.EventHandler(this.YDWM_BOX_TextChanged);
            // 
            // XDWM_BOX
            // 
            this.XDWM_BOX.Location = new System.Drawing.Point(397, 348);
            this.XDWM_BOX.Name = "XDWM_BOX";
            this.XDWM_BOX.Size = new System.Drawing.Size(28, 20);
            this.XDWM_BOX.TabIndex = 73;
            this.XDWM_BOX.TextChanged += new System.EventHandler(this.XDWM_BOX_TextChanged);
            // 
            // DS_CHECK
            // 
            this.DS_CHECK.AutoSize = true;
            this.DS_CHECK.Location = new System.Drawing.Point(578, 245);
            this.DS_CHECK.Name = "DS_CHECK";
            this.DS_CHECK.Size = new System.Drawing.Size(106, 17);
            this.DS_CHECK.TabIndex = 72;
            this.DS_CHECK.Text = "Дополнительно";
            this.DS_CHECK.UseVisualStyleBackColor = true;
            this.DS_CHECK.CheckedChanged += new System.EventHandler(this.DS_CHECK_CheckedChanged);
            // 
            // TEXTDS_BOX
            // 
            this.TEXTDS_BOX.Location = new System.Drawing.Point(578, 275);
            this.TEXTDS_BOX.Name = "TEXTDS_BOX";
            this.TEXTDS_BOX.Size = new System.Drawing.Size(130, 20);
            this.TEXTDS_BOX.TabIndex = 71;
            this.TEXTDS_BOX.TextChanged += new System.EventHandler(this.FDS_BOX_TextChanged);
            // 
            // SDS_BOX
            // 
            this.SDS_BOX.Location = new System.Drawing.Point(680, 311);
            this.SDS_BOX.Name = "SDS_BOX";
            this.SDS_BOX.Size = new System.Drawing.Size(28, 20);
            this.SDS_BOX.TabIndex = 70;
            this.SDS_BOX.TextChanged += new System.EventHandler(this.SDS_BOX_TextChanged);
            // 
            // FDS_BOX
            // 
            this.FDS_BOX.Location = new System.Drawing.Point(578, 311);
            this.FDS_BOX.Name = "FDS_BOX";
            this.FDS_BOX.Size = new System.Drawing.Size(96, 20);
            this.FDS_BOX.TabIndex = 69;
            this.FDS_BOX.TextChanged += new System.EventHandler(this.FDS_BOX_TextChanged_1);
            // 
            // HDSM_BOX
            // 
            this.HDSM_BOX.Location = new System.Drawing.Point(680, 348);
            this.HDSM_BOX.Name = "HDSM_BOX";
            this.HDSM_BOX.Size = new System.Drawing.Size(28, 20);
            this.HDSM_BOX.TabIndex = 68;
            this.HDSM_BOX.TextChanged += new System.EventHandler(this.HDSM_BOX_TextChanged);
            // 
            // WDSM_BOX
            // 
            this.WDSM_BOX.Location = new System.Drawing.Point(646, 348);
            this.WDSM_BOX.Name = "WDSM_BOX";
            this.WDSM_BOX.Size = new System.Drawing.Size(28, 20);
            this.WDSM_BOX.TabIndex = 67;
            this.WDSM_BOX.TextChanged += new System.EventHandler(this.WDSM_BOX_TextChanged);
            // 
            // YDSM_BOX
            // 
            this.YDSM_BOX.Location = new System.Drawing.Point(612, 348);
            this.YDSM_BOX.Name = "YDSM_BOX";
            this.YDSM_BOX.Size = new System.Drawing.Size(28, 20);
            this.YDSM_BOX.TabIndex = 66;
            this.YDSM_BOX.TextChanged += new System.EventHandler(this.YDSM_BOX_TextChanged);
            // 
            // XDSM_BOX
            // 
            this.XDSM_BOX.Location = new System.Drawing.Point(578, 348);
            this.XDSM_BOX.Name = "XDSM_BOX";
            this.XDSM_BOX.Size = new System.Drawing.Size(28, 20);
            this.XDSM_BOX.TabIndex = 65;
            this.XDSM_BOX.TextChanged += new System.EventHandler(this.XDSM_BOX_TextChanged);
            // 
            // DP_CHECK
            // 
            this.DP_CHECK.AutoSize = true;
            this.DP_CHECK.Location = new System.Drawing.Point(23, 245);
            this.DP_CHECK.Name = "DP_CHECK";
            this.DP_CHECK.Size = new System.Drawing.Size(106, 17);
            this.DP_CHECK.TabIndex = 64;
            this.DP_CHECK.Text = "Дополнительно";
            this.DP_CHECK.UseVisualStyleBackColor = true;
            this.DP_CHECK.CheckedChanged += new System.EventHandler(this.DP_CHECK_CheckedChanged);
            // 
            // TEXTDP_BOX
            // 
            this.TEXTDP_BOX.Location = new System.Drawing.Point(23, 275);
            this.TEXTDP_BOX.Name = "TEXTDP_BOX";
            this.TEXTDP_BOX.Size = new System.Drawing.Size(130, 20);
            this.TEXTDP_BOX.TabIndex = 63;
            this.TEXTDP_BOX.TextChanged += new System.EventHandler(this.TEXTDP_BOX_TextChanged);
            // 
            // SDP_BOX
            // 
            this.SDP_BOX.Location = new System.Drawing.Point(125, 311);
            this.SDP_BOX.Name = "SDP_BOX";
            this.SDP_BOX.Size = new System.Drawing.Size(28, 20);
            this.SDP_BOX.TabIndex = 62;
            this.SDP_BOX.TextChanged += new System.EventHandler(this.SDP_BOX_TextChanged);
            // 
            // FDP_BOX
            // 
            this.FDP_BOX.Location = new System.Drawing.Point(23, 311);
            this.FDP_BOX.Name = "FDP_BOX";
            this.FDP_BOX.Size = new System.Drawing.Size(96, 20);
            this.FDP_BOX.TabIndex = 61;
            this.FDP_BOX.TextChanged += new System.EventHandler(this.FDP_BOX_TextChanged);
            // 
            // HDPM_BOX
            // 
            this.HDPM_BOX.Location = new System.Drawing.Point(125, 348);
            this.HDPM_BOX.Name = "HDPM_BOX";
            this.HDPM_BOX.Size = new System.Drawing.Size(28, 20);
            this.HDPM_BOX.TabIndex = 60;
            this.HDPM_BOX.TextChanged += new System.EventHandler(this.HDPM_BOX_TextChanged);
            // 
            // WDPM_BOX
            // 
            this.WDPM_BOX.Location = new System.Drawing.Point(91, 348);
            this.WDPM_BOX.Name = "WDPM_BOX";
            this.WDPM_BOX.Size = new System.Drawing.Size(28, 20);
            this.WDPM_BOX.TabIndex = 59;
            this.WDPM_BOX.TextChanged += new System.EventHandler(this.WDPM_BOX_TextChanged);
            // 
            // YDPM_BOX
            // 
            this.YDPM_BOX.Location = new System.Drawing.Point(57, 348);
            this.YDPM_BOX.Name = "YDPM_BOX";
            this.YDPM_BOX.Size = new System.Drawing.Size(28, 20);
            this.YDPM_BOX.TabIndex = 58;
            this.YDPM_BOX.TextChanged += new System.EventHandler(this.YDPM_BOX_TextChanged);
            // 
            // XDPM_BOX
            // 
            this.XDPM_BOX.Location = new System.Drawing.Point(23, 348);
            this.XDPM_BOX.Name = "XDPM_BOX";
            this.XDPM_BOX.Size = new System.Drawing.Size(28, 20);
            this.XDPM_BOX.TabIndex = 57;
            this.XDPM_BOX.TextChanged += new System.EventHandler(this.XDPM_BOX_TextChanged);
            // 
            // DT_CHECK
            // 
            this.DT_CHECK.AutoSize = true;
            this.DT_CHECK.Location = new System.Drawing.Point(209, 245);
            this.DT_CHECK.Name = "DT_CHECK";
            this.DT_CHECK.Size = new System.Drawing.Size(106, 17);
            this.DT_CHECK.TabIndex = 56;
            this.DT_CHECK.Text = "Дополнительно";
            this.DT_CHECK.UseVisualStyleBackColor = true;
            this.DT_CHECK.CheckedChanged += new System.EventHandler(this.DT_CHECK_CheckedChanged);
            // 
            // TEXTDT_BOX
            // 
            this.TEXTDT_BOX.Location = new System.Drawing.Point(209, 275);
            this.TEXTDT_BOX.Name = "TEXTDT_BOX";
            this.TEXTDT_BOX.Size = new System.Drawing.Size(130, 20);
            this.TEXTDT_BOX.TabIndex = 54;
            this.TEXTDT_BOX.TextChanged += new System.EventHandler(this.textBox49_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(599, 142);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Значение";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(44, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Значение";
            // 
            // SSM_BOX
            // 
            this.SSM_BOX.Location = new System.Drawing.Point(680, 166);
            this.SSM_BOX.Name = "SSM_BOX";
            this.SSM_BOX.Size = new System.Drawing.Size(28, 20);
            this.SSM_BOX.TabIndex = 29;
            // 
            // FSM_BOX
            // 
            this.FSM_BOX.Location = new System.Drawing.Point(578, 166);
            this.FSM_BOX.Name = "FSM_BOX";
            this.FSM_BOX.Size = new System.Drawing.Size(96, 20);
            this.FSM_BOX.TabIndex = 28;
            // 
            // HSM_BOX
            // 
            this.HSM_BOX.Location = new System.Drawing.Point(680, 204);
            this.HSM_BOX.Name = "HSM_BOX";
            this.HSM_BOX.Size = new System.Drawing.Size(28, 20);
            this.HSM_BOX.TabIndex = 27;
            this.HSM_BOX.TextChanged += new System.EventHandler(this.HSM_BOX_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(407, 142);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Значение";
            // 
            // SWM_BOX
            // 
            this.SWM_BOX.Location = new System.Drawing.Point(499, 164);
            this.SWM_BOX.Name = "SWM_BOX";
            this.SWM_BOX.Size = new System.Drawing.Size(28, 20);
            this.SWM_BOX.TabIndex = 29;
            // 
            // SDT_BOX
            // 
            this.SDT_BOX.Location = new System.Drawing.Point(311, 311);
            this.SDT_BOX.Name = "SDT_BOX";
            this.SDT_BOX.Size = new System.Drawing.Size(28, 20);
            this.SDT_BOX.TabIndex = 23;
            this.SDT_BOX.TextChanged += new System.EventHandler(this.SDT_BOX_TextChanged);
            // 
            // FWM_BOX
            // 
            this.FWM_BOX.Location = new System.Drawing.Point(397, 164);
            this.FWM_BOX.Name = "FWM_BOX";
            this.FWM_BOX.Size = new System.Drawing.Size(96, 20);
            this.FWM_BOX.TabIndex = 28;
            // 
            // WSM_BOX
            // 
            this.WSM_BOX.Location = new System.Drawing.Point(646, 204);
            this.WSM_BOX.Name = "WSM_BOX";
            this.WSM_BOX.Size = new System.Drawing.Size(28, 20);
            this.WSM_BOX.TabIndex = 26;
            // 
            // HWM_BOX
            // 
            this.HWM_BOX.Location = new System.Drawing.Point(499, 204);
            this.HWM_BOX.Name = "HWM_BOX";
            this.HWM_BOX.Size = new System.Drawing.Size(28, 20);
            this.HWM_BOX.TabIndex = 27;
            // 
            // WWM_BOX
            // 
            this.WWM_BOX.Location = new System.Drawing.Point(465, 204);
            this.WWM_BOX.Name = "WWM_BOX";
            this.WWM_BOX.Size = new System.Drawing.Size(28, 20);
            this.WWM_BOX.TabIndex = 26;
            // 
            // YSM_BOX
            // 
            this.YSM_BOX.Location = new System.Drawing.Point(612, 204);
            this.YSM_BOX.Name = "YSM_BOX";
            this.YSM_BOX.Size = new System.Drawing.Size(28, 20);
            this.YSM_BOX.TabIndex = 25;
            this.YSM_BOX.TextChanged += new System.EventHandler(this.YSM_BOX_TextChanged);
            // 
            // YWM_BOX
            // 
            this.YWM_BOX.Location = new System.Drawing.Point(431, 204);
            this.YWM_BOX.Name = "YWM_BOX";
            this.YWM_BOX.Size = new System.Drawing.Size(28, 20);
            this.YWM_BOX.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(234, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Значение";
            this.label4.Click += new System.EventHandler(this.label4_Click_1);
            // 
            // XWM_BOX
            // 
            this.XWM_BOX.Location = new System.Drawing.Point(397, 204);
            this.XWM_BOX.Name = "XWM_BOX";
            this.XWM_BOX.Size = new System.Drawing.Size(28, 20);
            this.XWM_BOX.TabIndex = 24;
            // 
            // XSM_BOX
            // 
            this.XSM_BOX.Location = new System.Drawing.Point(578, 204);
            this.XSM_BOX.Name = "XSM_BOX";
            this.XSM_BOX.Size = new System.Drawing.Size(28, 20);
            this.XSM_BOX.TabIndex = 24;
            this.XSM_BOX.TextChanged += new System.EventHandler(this.XSM_BOX_TextChanged);
            // 
            // SW_BOX
            // 
            this.SW_BOX.Location = new System.Drawing.Point(499, 63);
            this.SW_BOX.Name = "SW_BOX";
            this.SW_BOX.Size = new System.Drawing.Size(28, 20);
            this.SW_BOX.TabIndex = 23;
            // 
            // SS_BOX
            // 
            this.SS_BOX.Location = new System.Drawing.Point(683, 63);
            this.SS_BOX.Name = "SS_BOX";
            this.SS_BOX.Size = new System.Drawing.Size(28, 20);
            this.SS_BOX.TabIndex = 23;
            // 
            // FW_BOX
            // 
            this.FW_BOX.Location = new System.Drawing.Point(397, 63);
            this.FW_BOX.Name = "FW_BOX";
            this.FW_BOX.Size = new System.Drawing.Size(96, 20);
            this.FW_BOX.TabIndex = 22;
            // 
            // FDT_BOX
            // 
            this.FDT_BOX.Location = new System.Drawing.Point(209, 311);
            this.FDT_BOX.Name = "FDT_BOX";
            this.FDT_BOX.Size = new System.Drawing.Size(96, 20);
            this.FDT_BOX.TabIndex = 22;
            this.FDT_BOX.TextChanged += new System.EventHandler(this.FDT_BOX_TextChanged);
            // 
            // HW_BOX
            // 
            this.HW_BOX.Location = new System.Drawing.Point(499, 104);
            this.HW_BOX.Name = "HW_BOX";
            this.HW_BOX.Size = new System.Drawing.Size(28, 20);
            this.HW_BOX.TabIndex = 4;
            this.HW_BOX.TextChanged += new System.EventHandler(this.HW_BOX_TextChanged);
            // 
            // FS_BOX
            // 
            this.FS_BOX.Location = new System.Drawing.Point(581, 63);
            this.FS_BOX.Name = "FS_BOX";
            this.FS_BOX.Size = new System.Drawing.Size(96, 20);
            this.FS_BOX.TabIndex = 22;
            // 
            // WW_BOX
            // 
            this.WW_BOX.Location = new System.Drawing.Point(465, 104);
            this.WW_BOX.Name = "WW_BOX";
            this.WW_BOX.Size = new System.Drawing.Size(28, 20);
            this.WW_BOX.TabIndex = 3;
            this.WW_BOX.TextChanged += new System.EventHandler(this.WW_BOX_TextChanged);
            // 
            // YW_BOX
            // 
            this.YW_BOX.Location = new System.Drawing.Point(431, 104);
            this.YW_BOX.Name = "YW_BOX";
            this.YW_BOX.Size = new System.Drawing.Size(28, 20);
            this.YW_BOX.TabIndex = 2;
            // 
            // HS_BOX
            // 
            this.HS_BOX.Location = new System.Drawing.Point(683, 104);
            this.HS_BOX.Name = "HS_BOX";
            this.HS_BOX.Size = new System.Drawing.Size(28, 20);
            this.HS_BOX.TabIndex = 4;
            // 
            // XW_BOX
            // 
            this.XW_BOX.Location = new System.Drawing.Point(397, 104);
            this.XW_BOX.Name = "XW_BOX";
            this.XW_BOX.Size = new System.Drawing.Size(28, 20);
            this.XW_BOX.TabIndex = 1;
            this.XW_BOX.TextChanged += new System.EventHandler(this.XW_BOX_TextChanged);
            // 
            // STM_BOX
            // 
            this.STM_BOX.Location = new System.Drawing.Point(312, 166);
            this.STM_BOX.Name = "STM_BOX";
            this.STM_BOX.Size = new System.Drawing.Size(28, 20);
            this.STM_BOX.TabIndex = 29;
            this.STM_BOX.TextChanged += new System.EventHandler(this.STM_BOX_TextChanged);
            // 
            // W_CHECK
            // 
            this.W_CHECK.AutoSize = true;
            this.W_CHECK.Location = new System.Drawing.Point(399, 23);
            this.W_CHECK.Name = "W_CHECK";
            this.W_CHECK.Size = new System.Drawing.Size(82, 17);
            this.W_CHECK.TabIndex = 0;
            this.W_CHECK.Text = "Влажность";
            this.W_CHECK.UseVisualStyleBackColor = true;
            this.W_CHECK.CheckedChanged += new System.EventHandler(this.W_CHECK_CheckedChanged);
            // 
            // WS_BOX
            // 
            this.WS_BOX.Location = new System.Drawing.Point(649, 104);
            this.WS_BOX.Name = "WS_BOX";
            this.WS_BOX.Size = new System.Drawing.Size(28, 20);
            this.WS_BOX.TabIndex = 3;
            // 
            // YS_BOX
            // 
            this.YS_BOX.Location = new System.Drawing.Point(615, 104);
            this.YS_BOX.Name = "YS_BOX";
            this.YS_BOX.Size = new System.Drawing.Size(28, 20);
            this.YS_BOX.TabIndex = 2;
            // 
            // HDTM_BOX
            // 
            this.HDTM_BOX.Location = new System.Drawing.Point(311, 348);
            this.HDTM_BOX.Name = "HDTM_BOX";
            this.HDTM_BOX.Size = new System.Drawing.Size(28, 20);
            this.HDTM_BOX.TabIndex = 4;
            this.HDTM_BOX.TextChanged += new System.EventHandler(this.HDTM_BOX_TextChanged);
            // 
            // XS_BOX
            // 
            this.XS_BOX.Location = new System.Drawing.Point(581, 104);
            this.XS_BOX.Name = "XS_BOX";
            this.XS_BOX.Size = new System.Drawing.Size(28, 20);
            this.XS_BOX.TabIndex = 1;
            // 
            // S_CHECK
            // 
            this.S_CHECK.AutoSize = true;
            this.S_CHECK.Location = new System.Drawing.Point(583, 23);
            this.S_CHECK.Name = "S_CHECK";
            this.S_CHECK.Size = new System.Drawing.Size(56, 17);
            this.S_CHECK.TabIndex = 0;
            this.S_CHECK.Text = "Ветер";
            this.S_CHECK.UseVisualStyleBackColor = true;
            this.S_CHECK.CheckedChanged += new System.EventHandler(this.S_CHECK_CheckedChanged);
            // 
            // FTM_BOX
            // 
            this.FTM_BOX.Location = new System.Drawing.Point(210, 166);
            this.FTM_BOX.Name = "FTM_BOX";
            this.FTM_BOX.Size = new System.Drawing.Size(96, 20);
            this.FTM_BOX.TabIndex = 28;
            this.FTM_BOX.TextChanged += new System.EventHandler(this.FTM_BOX_TextChanged);
            // 
            // WDTM_BOX
            // 
            this.WDTM_BOX.Location = new System.Drawing.Point(277, 348);
            this.WDTM_BOX.Name = "WDTM_BOX";
            this.WDTM_BOX.Size = new System.Drawing.Size(28, 20);
            this.WDTM_BOX.TabIndex = 3;
            this.WDTM_BOX.TextChanged += new System.EventHandler(this.WDTM_BOX_TextChanged);
            // 
            // HTM_BOX
            // 
            this.HTM_BOX.Location = new System.Drawing.Point(312, 204);
            this.HTM_BOX.Name = "HTM_BOX";
            this.HTM_BOX.Size = new System.Drawing.Size(28, 20);
            this.HTM_BOX.TabIndex = 27;
            this.HTM_BOX.TextChanged += new System.EventHandler(this.HTM_BOX_TextChanged);
            // 
            // YDTM_BOX
            // 
            this.YDTM_BOX.Location = new System.Drawing.Point(243, 348);
            this.YDTM_BOX.Name = "YDTM_BOX";
            this.YDTM_BOX.Size = new System.Drawing.Size(28, 20);
            this.YDTM_BOX.TabIndex = 2;
            this.YDTM_BOX.TextChanged += new System.EventHandler(this.YDTM_BOX_TextChanged);
            // 
            // WTM_BOX
            // 
            this.WTM_BOX.Location = new System.Drawing.Point(278, 204);
            this.WTM_BOX.Name = "WTM_BOX";
            this.WTM_BOX.Size = new System.Drawing.Size(28, 20);
            this.WTM_BOX.TabIndex = 26;
            this.WTM_BOX.TextChanged += new System.EventHandler(this.WTM_BOX_TextChanged);
            // 
            // XDTM_BOX
            // 
            this.XDTM_BOX.Location = new System.Drawing.Point(209, 348);
            this.XDTM_BOX.Name = "XDTM_BOX";
            this.XDTM_BOX.Size = new System.Drawing.Size(28, 20);
            this.XDTM_BOX.TabIndex = 1;
            this.XDTM_BOX.TextChanged += new System.EventHandler(this.XDTM_BOX_TextChanged);
            // 
            // YTM_BOX
            // 
            this.YTM_BOX.Location = new System.Drawing.Point(244, 204);
            this.YTM_BOX.Name = "YTM_BOX";
            this.YTM_BOX.Size = new System.Drawing.Size(28, 20);
            this.YTM_BOX.TabIndex = 25;
            this.YTM_BOX.TextChanged += new System.EventHandler(this.YTM_BOX_TextChanged);
            // 
            // XTM_BOX
            // 
            this.XTM_BOX.Location = new System.Drawing.Point(210, 204);
            this.XTM_BOX.Name = "XTM_BOX";
            this.XTM_BOX.Size = new System.Drawing.Size(28, 20);
            this.XTM_BOX.TabIndex = 24;
            this.XTM_BOX.TextChanged += new System.EventHandler(this.XTM_BOX_TextChanged);
            // 
            // ST_BOX
            // 
            this.ST_BOX.Location = new System.Drawing.Point(313, 63);
            this.ST_BOX.Name = "ST_BOX";
            this.ST_BOX.Size = new System.Drawing.Size(28, 20);
            this.ST_BOX.TabIndex = 23;
            this.ST_BOX.TextChanged += new System.EventHandler(this.ST_BOX_TextChanged);
            // 
            // FT_BOX
            // 
            this.FT_BOX.Location = new System.Drawing.Point(211, 63);
            this.FT_BOX.Name = "FT_BOX";
            this.FT_BOX.Size = new System.Drawing.Size(96, 20);
            this.FT_BOX.TabIndex = 22;
            this.FT_BOX.TextChanged += new System.EventHandler(this.FT_BOX_TextChanged);
            // 
            // HT_BOX
            // 
            this.HT_BOX.Location = new System.Drawing.Point(313, 104);
            this.HT_BOX.Name = "HT_BOX";
            this.HT_BOX.Size = new System.Drawing.Size(28, 20);
            this.HT_BOX.TabIndex = 4;
            this.HT_BOX.TextChanged += new System.EventHandler(this.HT_BOX_TextChanged);
            // 
            // WT_BOX
            // 
            this.WT_BOX.Location = new System.Drawing.Point(279, 104);
            this.WT_BOX.Name = "WT_BOX";
            this.WT_BOX.Size = new System.Drawing.Size(28, 20);
            this.WT_BOX.TabIndex = 3;
            this.WT_BOX.TextChanged += new System.EventHandler(this.WT_BOX_TextChanged);
            // 
            // YT_BOX
            // 
            this.YT_BOX.Location = new System.Drawing.Point(245, 104);
            this.YT_BOX.Name = "YT_BOX";
            this.YT_BOX.Size = new System.Drawing.Size(28, 20);
            this.YT_BOX.TabIndex = 2;
            this.YT_BOX.TextChanged += new System.EventHandler(this.YT_BOX_TextChanged);
            // 
            // XT_BOX
            // 
            this.XT_BOX.Location = new System.Drawing.Point(211, 104);
            this.XT_BOX.Name = "XT_BOX";
            this.XT_BOX.Size = new System.Drawing.Size(28, 20);
            this.XT_BOX.TabIndex = 1;
            this.XT_BOX.TextChanged += new System.EventHandler(this.XT_BOX_TextChanged);
            // 
            // TEMP_CHECK
            // 
            this.TEMP_CHECK.AutoSize = true;
            this.TEMP_CHECK.Location = new System.Drawing.Point(217, 24);
            this.TEMP_CHECK.Name = "TEMP_CHECK";
            this.TEMP_CHECK.Size = new System.Drawing.Size(93, 17);
            this.TEMP_CHECK.TabIndex = 0;
            this.TEMP_CHECK.Text = "Температура";
            this.TEMP_CHECK.UseVisualStyleBackColor = true;
            this.TEMP_CHECK.CheckedChanged += new System.EventHandler(this.TEMP_CHECK_CheckedChanged);
            // 
            // Start
            // 
            this.Start.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Start.Location = new System.Drawing.Point(10, 498);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(171, 31);
            this.Start.TabIndex = 0;
            this.Start.Text = "Конфигурация";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // Message
            // 
            this.Message.CausesValidation = false;
            this.Message.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Message.Location = new System.Drawing.Point(12, 535);
            this.Message.Multiline = true;
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(908, 102);
            this.Message.TabIndex = 38;
            this.Message.TextChanged += new System.EventHandler(this.Message_TextChanged_1);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.Controls.Add(this.ADRES_SET);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.StartDinamicA);
            this.groupBox1.Controls.Add(this.groupBox8);
            this.groupBox1.Controls.Add(this.Send_Info);
            this.groupBox1.Controls.Add(this.Start);
            this.groupBox1.Controls.Add(this.DellDinamicA);
            this.groupBox1.Controls.Add(this.Save_data);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.CLOCK);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(933, 659);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // ADRES_SET
            // 
            this.ADRES_SET.FormattingEnabled = true;
            this.ADRES_SET.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.ADRES_SET.Location = new System.Drawing.Point(86, 24);
            this.ADRES_SET.Name = "ADRES_SET";
            this.ADRES_SET.Size = new System.Drawing.Size(35, 21);
            this.ADRES_SET.TabIndex = 36;
            this.ADRES_SET.SelectedIndexChanged += new System.EventHandler(this.ADRES_SET_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.DinamicT_6);
            this.groupBox4.Controls.Add(this.DinamicT_1);
            this.groupBox4.Controls.Add(this.DinamicT_5);
            this.groupBox4.Controls.Add(this.DinamicT_2);
            this.groupBox4.Controls.Add(this.DinamicT_4);
            this.groupBox4.Controls.Add(this.DinamicT_3);
            this.groupBox4.Location = new System.Drawing.Point(185, 444);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(734, 44);
            this.groupBox4.TabIndex = 33;
            this.groupBox4.TabStop = false;
            // 
            // DinamicT_6
            // 
            this.DinamicT_6.AutoSize = true;
            this.DinamicT_6.Location = new System.Drawing.Point(653, 17);
            this.DinamicT_6.Name = "DinamicT_6";
            this.DinamicT_6.Size = new System.Drawing.Size(66, 17);
            this.DinamicT_6.TabIndex = 38;
            this.DinamicT_6.Text = "Табло 6";
            this.DinamicT_6.UseVisualStyleBackColor = true;
            this.DinamicT_6.CheckedChanged += new System.EventHandler(this.DinamicT_6_CheckedChanged);
            // 
            // DinamicT_1
            // 
            this.DinamicT_1.AutoSize = true;
            this.DinamicT_1.Location = new System.Drawing.Point(16, 18);
            this.DinamicT_1.Name = "DinamicT_1";
            this.DinamicT_1.Size = new System.Drawing.Size(66, 17);
            this.DinamicT_1.TabIndex = 0;
            this.DinamicT_1.Text = "Табло 1";
            this.DinamicT_1.UseVisualStyleBackColor = true;
            this.DinamicT_1.CheckedChanged += new System.EventHandler(this.DinamicT_1_CheckedChanged);
            // 
            // DinamicT_5
            // 
            this.DinamicT_5.AutoSize = true;
            this.DinamicT_5.Location = new System.Drawing.Point(533, 18);
            this.DinamicT_5.Name = "DinamicT_5";
            this.DinamicT_5.Size = new System.Drawing.Size(66, 17);
            this.DinamicT_5.TabIndex = 37;
            this.DinamicT_5.Text = "Табло 5";
            this.DinamicT_5.UseVisualStyleBackColor = true;
            // 
            // DinamicT_2
            // 
            this.DinamicT_2.AutoSize = true;
            this.DinamicT_2.Location = new System.Drawing.Point(140, 19);
            this.DinamicT_2.Name = "DinamicT_2";
            this.DinamicT_2.Size = new System.Drawing.Size(66, 17);
            this.DinamicT_2.TabIndex = 34;
            this.DinamicT_2.Text = "Табло 2";
            this.DinamicT_2.UseVisualStyleBackColor = true;
            this.DinamicT_2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // DinamicT_4
            // 
            this.DinamicT_4.AutoSize = true;
            this.DinamicT_4.Location = new System.Drawing.Point(397, 18);
            this.DinamicT_4.Name = "DinamicT_4";
            this.DinamicT_4.Size = new System.Drawing.Size(66, 17);
            this.DinamicT_4.TabIndex = 36;
            this.DinamicT_4.Text = "Табло 4";
            this.DinamicT_4.UseVisualStyleBackColor = true;
            // 
            // DinamicT_3
            // 
            this.DinamicT_3.AutoSize = true;
            this.DinamicT_3.Location = new System.Drawing.Point(261, 18);
            this.DinamicT_3.Name = "DinamicT_3";
            this.DinamicT_3.Size = new System.Drawing.Size(66, 17);
            this.DinamicT_3.TabIndex = 35;
            this.DinamicT_3.Text = "Табло 3";
            this.DinamicT_3.UseVisualStyleBackColor = true;
            // 
            // StartDinamicA
            // 
            this.StartDinamicA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StartDinamicA.Location = new System.Drawing.Point(369, 498);
            this.StartDinamicA.Name = "StartDinamicA";
            this.StartDinamicA.Size = new System.Drawing.Size(185, 31);
            this.StartDinamicA.TabIndex = 43;
            this.StartDinamicA.Text = "Создать области";
            this.StartDinamicA.UseVisualStyleBackColor = true;
            this.StartDinamicA.Click += new System.EventHandler(this.StartDinamicA_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.SD_BOX);
            this.groupBox8.Controls.Add(this.FD_BOX);
            this.groupBox8.Controls.Add(this.HD_BOX);
            this.groupBox8.Controls.Add(this.WD_BOX);
            this.groupBox8.Controls.Add(this.YD_BOX);
            this.groupBox8.Controls.Add(this.XD_BOX);
            this.groupBox8.Controls.Add(this.D_CHECK);
            this.groupBox8.Location = new System.Drawing.Point(13, 63);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(167, 140);
            this.groupBox8.TabIndex = 42;
            this.groupBox8.TabStop = false;
            // 
            // SD_BOX
            // 
            this.SD_BOX.Location = new System.Drawing.Point(121, 64);
            this.SD_BOX.Name = "SD_BOX";
            this.SD_BOX.Size = new System.Drawing.Size(28, 20);
            this.SD_BOX.TabIndex = 23;
            // 
            // FD_BOX
            // 
            this.FD_BOX.Location = new System.Drawing.Point(19, 64);
            this.FD_BOX.Name = "FD_BOX";
            this.FD_BOX.Size = new System.Drawing.Size(96, 20);
            this.FD_BOX.TabIndex = 22;
            this.FD_BOX.TextChanged += new System.EventHandler(this.FD_BOX_TextChanged);
            // 
            // HD_BOX
            // 
            this.HD_BOX.Location = new System.Drawing.Point(121, 111);
            this.HD_BOX.Name = "HD_BOX";
            this.HD_BOX.Size = new System.Drawing.Size(28, 20);
            this.HD_BOX.TabIndex = 4;
            // 
            // WD_BOX
            // 
            this.WD_BOX.Location = new System.Drawing.Point(87, 111);
            this.WD_BOX.Name = "WD_BOX";
            this.WD_BOX.Size = new System.Drawing.Size(28, 20);
            this.WD_BOX.TabIndex = 3;
            this.WD_BOX.TextChanged += new System.EventHandler(this.WD_BOX_TextChanged);
            // 
            // YD_BOX
            // 
            this.YD_BOX.Location = new System.Drawing.Point(53, 111);
            this.YD_BOX.Name = "YD_BOX";
            this.YD_BOX.Size = new System.Drawing.Size(28, 20);
            this.YD_BOX.TabIndex = 2;
            // 
            // XD_BOX
            // 
            this.XD_BOX.Location = new System.Drawing.Point(19, 111);
            this.XD_BOX.Name = "XD_BOX";
            this.XD_BOX.Size = new System.Drawing.Size(28, 20);
            this.XD_BOX.TabIndex = 1;
            // 
            // D_CHECK
            // 
            this.D_CHECK.AutoSize = true;
            this.D_CHECK.Location = new System.Drawing.Point(21, 25);
            this.D_CHECK.Name = "D_CHECK";
            this.D_CHECK.Size = new System.Drawing.Size(47, 17);
            this.D_CHECK.TabIndex = 0;
            this.D_CHECK.Text = "Дни";
            this.D_CHECK.UseVisualStyleBackColor = true;
            this.D_CHECK.CheckedChanged += new System.EventHandler(this.Days_CheckedChanged);
            // 
            // Send_Info
            // 
            this.Send_Info.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Send_Info.Location = new System.Drawing.Point(737, 498);
            this.Send_Info.Name = "Send_Info";
            this.Send_Info.Size = new System.Drawing.Size(183, 31);
            this.Send_Info.TabIndex = 41;
            this.Send_Info.Text = "Записать параметры";
            this.Send_Info.UseVisualStyleBackColor = true;
            this.Send_Info.Click += new System.EventHandler(this.Send_Info_Click);
            // 
            // DellDinamicA
            // 
            this.DellDinamicA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DellDinamicA.Location = new System.Drawing.Point(555, 498);
            this.DellDinamicA.Name = "DellDinamicA";
            this.DellDinamicA.Size = new System.Drawing.Size(181, 31);
            this.DellDinamicA.TabIndex = 40;
            this.DellDinamicA.Text = "Удалить области";
            this.DellDinamicA.UseVisualStyleBackColor = true;
            this.DellDinamicA.Click += new System.EventHandler(this.DellDinamicA_Click);
            // 
            // Save_data
            // 
            this.Save_data.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Save_data.Location = new System.Drawing.Point(182, 498);
            this.Save_data.Name = "Save_data";
            this.Save_data.Size = new System.Drawing.Size(186, 31);
            this.Save_data.TabIndex = 39;
            this.Save_data.Text = "Сохранить ";
            this.Save_data.UseVisualStyleBackColor = true;
            this.Save_data.Click += new System.EventHandler(this.Save_data_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(933, 659);
            this.Controls.Add(this.Message);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.CLOCK.ResumeLayout(false);
            this.CLOCK.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.CheckBox WRez;
        private System.Windows.Forms.TextBox SizeFont_WRez_T6;
        private System.Windows.Forms.TextBox Font_WRez_T6;
        private System.Windows.Forms.TextBox H_WRez_T6;
        private System.Windows.Forms.TextBox W_WRez_T6;
        private System.Windows.Forms.TextBox Y_WRez_T6;
        private System.Windows.Forms.TextBox X_WRez_T6;
        private System.Windows.Forms.TextBox SizeFont_W_T6;
        private System.Windows.Forms.TextBox Font_W_T6;
        private System.Windows.Forms.TextBox H_W_T6;
        private System.Windows.Forms.TextBox W_W_T6;
        private System.Windows.Forms.TextBox Y_W_T6;
        private System.Windows.Forms.TextBox X_W_T6;
        private System.Windows.Forms.CheckBox Add_W;
        private System.Windows.Forms.CheckBox TempRez_T6;
        private System.Windows.Forms.TextBox SizeFont_TempRez_T6;
        private System.Windows.Forms.TextBox Font_TempRez_T6;
        private System.Windows.Forms.TextBox H_TempRez_T6;
        private System.Windows.Forms.TextBox W_TempRez_T6;
        private System.Windows.Forms.TextBox Y_TempRez_T6;
        private System.Windows.Forms.TextBox X_TempRez_T6;
        private System.Windows.Forms.TextBox SizeFont_Temp_T6;
        private System.Windows.Forms.TextBox Font_Temp_T6;
        private System.Windows.Forms.TextBox H_Temp_T6;
        private System.Windows.Forms.TextBox W_Temp_T6;
        private System.Windows.Forms.TextBox Y_Temp_T6;
        private System.Windows.Forms.TextBox X_Temp_T6;
        private System.Windows.Forms.CheckBox Add_Temp;
        private System.Windows.Forms.CheckBox radioButton2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.CheckBox radioButton3;
        private System.Windows.Forms.CheckBox radioButton6;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.CheckBox radioButton7;
        private System.Windows.Forms.TextBox SizeFont_Calendar_T6;
        private System.Windows.Forms.TextBox Font_Calendar_T6;
        private System.Windows.Forms.TextBox H_Calendar_T6;
        private System.Windows.Forms.TextBox W_Calendar_T6;
        private System.Windows.Forms.TextBox Y_Calendar_T6;
        private System.Windows.Forms.TextBox X_Calendar_T6;
        private System.Windows.Forms.CheckBox Add_Calendar;
        private System.Windows.Forms.CheckBox radioButton1;
        private System.Windows.Forms.TextBox IP6_TEXT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ID6_TEXT;
        private System.Windows.Forms.Label label_IP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PORT6_TEXT;
        private System.Windows.Forms.TextBox SizeFont_Clock_T6;
        private System.Windows.Forms.TextBox Font_Clock_T6;
        private System.Windows.Forms.TextBox H_Clock_T6;
        private System.Windows.Forms.TextBox W_Clock_T6;
        private System.Windows.Forms.TextBox Y_Clock_T6;
        private System.Windows.Forms.TextBox X_Clock_T6;
        private System.Windows.Forms.CheckBox Add_Clock;
        private System.Windows.Forms.GroupBox CLOCK;
        private System.Windows.Forms.TextBox SC_BOX;
        private System.Windows.Forms.TextBox FC_BOX;
        private System.Windows.Forms.TextBox HC_BOX;
        private System.Windows.Forms.TextBox WC_BOX;
        private System.Windows.Forms.TextBox YC_BOX;
        private System.Windows.Forms.TextBox XC_BOX;
        private System.Windows.Forms.CheckBox CLOCK_CHECK;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox COM_BOX;
        private System.Windows.Forms.Label ComLable;
        private System.Windows.Forms.TextBox IP_BOX;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ID_BOX;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox PORT_BOX;
        private System.Windows.Forms.TextBox HV_BOX;
        private System.Windows.Forms.TextBox TV_BOX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PV_BOX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox SK_BOX;
        private System.Windows.Forms.TextBox FK_BOX;
        private System.Windows.Forms.TextBox HK_BOX;
        private System.Windows.Forms.TextBox WK_BOX;
        private System.Windows.Forms.TextBox YK_BOX;
        private System.Windows.Forms.TextBox XK_BOX;
        private System.Windows.Forms.CheckBox K_CHECK;
        private System.Windows.Forms.TextBox SPM_BOX;
        private System.Windows.Forms.TextBox FPM_BOX;
        private System.Windows.Forms.TextBox HPM_BOX;
        private System.Windows.Forms.TextBox WPM_BOX;
        private System.Windows.Forms.TextBox YPM_BOX;
        private System.Windows.Forms.TextBox XPM_BOX;
        private System.Windows.Forms.TextBox SP_BOX;
        private System.Windows.Forms.TextBox FP_BOX;
        private System.Windows.Forms.TextBox HP_BOX;
        private System.Windows.Forms.TextBox WP_BOX;
        private System.Windows.Forms.TextBox YP_BOX;
        private System.Windows.Forms.TextBox XP_BOX;
        private System.Windows.Forms.CheckBox PRES_CHECK;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox STM_BOX;
        private System.Windows.Forms.TextBox FTM_BOX;
        private System.Windows.Forms.TextBox HTM_BOX;
        private System.Windows.Forms.TextBox WTM_BOX;
        private System.Windows.Forms.TextBox YTM_BOX;
        private System.Windows.Forms.TextBox XTM_BOX;
        private System.Windows.Forms.TextBox ST_BOX;
        private System.Windows.Forms.TextBox FT_BOX;
        private System.Windows.Forms.TextBox HT_BOX;
        private System.Windows.Forms.TextBox WT_BOX;
        private System.Windows.Forms.TextBox YT_BOX;
        private System.Windows.Forms.TextBox XT_BOX;
        private System.Windows.Forms.CheckBox TEMP_CHECK;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.TextBox SW_BOX;
        private System.Windows.Forms.TextBox FW_BOX;
        private System.Windows.Forms.TextBox HW_BOX;
        private System.Windows.Forms.TextBox WW_BOX;
        private System.Windows.Forms.TextBox YW_BOX;
        private System.Windows.Forms.TextBox XW_BOX;
        private System.Windows.Forms.CheckBox W_CHECK;
        private System.Windows.Forms.TextBox Message;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Send_Info;
        private System.Windows.Forms.Button DellDinamicA;
        private System.Windows.Forms.Button Save_data;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox SV_BOX;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox SD_BOX;
        private System.Windows.Forms.TextBox FD_BOX;
        private System.Windows.Forms.TextBox HD_BOX;
        private System.Windows.Forms.TextBox WD_BOX;
        private System.Windows.Forms.TextBox YD_BOX;
        private System.Windows.Forms.TextBox XD_BOX;
        private System.Windows.Forms.CheckBox D_CHECK;
        private System.Windows.Forms.TextBox W_BOX;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox H_BOX;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TIMER_BOX;
        private System.Windows.Forms.TextBox SDT_BOX;
        private System.Windows.Forms.TextBox FDT_BOX;
        private System.Windows.Forms.TextBox HDTM_BOX;
        private System.Windows.Forms.TextBox WDTM_BOX;
        private System.Windows.Forms.TextBox YDTM_BOX;
        private System.Windows.Forms.TextBox XDTM_BOX;
        private System.Windows.Forms.Button StartDinamicA;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox SSM_BOX;
        private System.Windows.Forms.TextBox FSM_BOX;
        private System.Windows.Forms.TextBox HSM_BOX;
        private System.Windows.Forms.TextBox WSM_BOX;
        private System.Windows.Forms.TextBox YSM_BOX;
        private System.Windows.Forms.TextBox XSM_BOX;
        private System.Windows.Forms.TextBox SS_BOX;
        private System.Windows.Forms.TextBox FS_BOX;
        private System.Windows.Forms.TextBox HS_BOX;
        private System.Windows.Forms.TextBox WS_BOX;
        private System.Windows.Forms.TextBox YS_BOX;
        private System.Windows.Forms.TextBox XS_BOX;
        private System.Windows.Forms.CheckBox S_CHECK;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox SWM_BOX;
        private System.Windows.Forms.TextBox FWM_BOX;
        private System.Windows.Forms.TextBox HWM_BOX;
        private System.Windows.Forms.TextBox WWM_BOX;
        private System.Windows.Forms.TextBox YWM_BOX;
        private System.Windows.Forms.TextBox XWM_BOX;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox DinamicT_1;
        private System.Windows.Forms.CheckBox DinamicT_6;
        private System.Windows.Forms.CheckBox DinamicT_5;
        private System.Windows.Forms.CheckBox DinamicT_4;
        private System.Windows.Forms.CheckBox DinamicT_3;
        private System.Windows.Forms.CheckBox DinamicT_2;
        private System.Windows.Forms.TextBox TEXTDT_BOX;
        private System.Windows.Forms.CheckBox DT_CHECK;
        private System.Windows.Forms.CheckBox DW_CHECK;
        private System.Windows.Forms.TextBox TEXTDW_BOX;
        private System.Windows.Forms.TextBox SDW_BOX;
        private System.Windows.Forms.TextBox FDW_BOX;
        private System.Windows.Forms.TextBox HDWM_BOX;
        private System.Windows.Forms.TextBox WDWM_BOX;
        private System.Windows.Forms.TextBox YDWM_BOX;
        private System.Windows.Forms.TextBox XDWM_BOX;
        private System.Windows.Forms.CheckBox DS_CHECK;
        private System.Windows.Forms.TextBox TEXTDS_BOX;
        private System.Windows.Forms.TextBox SDS_BOX;
        private System.Windows.Forms.TextBox FDS_BOX;
        private System.Windows.Forms.TextBox HDSM_BOX;
        private System.Windows.Forms.TextBox WDSM_BOX;
        private System.Windows.Forms.TextBox YDSM_BOX;
        private System.Windows.Forms.TextBox XDSM_BOX;
        private System.Windows.Forms.CheckBox DP_CHECK;
        private System.Windows.Forms.TextBox TEXTDP_BOX;
        private System.Windows.Forms.TextBox SDP_BOX;
        private System.Windows.Forms.TextBox FDP_BOX;
        private System.Windows.Forms.TextBox HDPM_BOX;
        private System.Windows.Forms.TextBox WDPM_BOX;
        private System.Windows.Forms.TextBox YDPM_BOX;
        private System.Windows.Forms.TextBox XDPM_BOX;
        private System.Windows.Forms.ComboBox ADRES_SET;
    }
}

